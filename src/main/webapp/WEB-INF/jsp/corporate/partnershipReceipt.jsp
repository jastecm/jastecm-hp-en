<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
$(document).ready(function(){
	$("#btn_send").on("click",function(){
		var frmParam = $("#frm").serializeArray();
		var param = {};
		for(var i= 0 ; i < frmParam.length ; i++){
			if(param[frmParam[i].name]) param[frmParam[i].name] += ","+frmParam[i].value
			else param[frmParam[i].name] = frmParam[i].value
		}
		param.callPage = "/corporate/ReceiptFinish";
		var url = "/index.do";
		$JBIZ.instance_post(url,param);
	});
});
</script>
	<div class="sub_layout">
		<div class="sub_title5 mab68">
			Receipt Confirmation
		</div>
		<form id="frm">
		<div class="sub_tit1">Proposal Description</div>
		<table class="table2 table2_2 mab76">
			<colgroup>
				<col width="88" />
				<col width="*" />
				<col width="74" />
				
			</colgroup>
			<tbody>
				<tr>
					<th>Title</th>
					<td>${reqVo.name }<input type="hidden" name="name" value="${reqVo.name}"></td>
					<td>Section
					 ${reqVo.partnershipType }<input type="hidden" name="partnershipType" value="${reqVo.partnershipType}"></td>
				</tr>
				</tr>
				<tr>
					<th>Description
					</th>
					<td colspan="2">${reqVo.contents}<input type="hidden" name="contents" value="${reqVo.contents}"></td>
				</tr>
				<tr>
					<th>File</th>
					<td colspan="2">
						<c:set var="fileList" value="${fn:split(reqVo.file,',')}" />
						<c:set var="fileNmList" value="${fn:split(reqVo.fileNm,',')}" />
						<c:forEach var="vo" items="${fileList}" varStatus="i">
							<input type="hidden" name="file" value="${vo}">
						</c:forEach>
						<c:forEach var="vo" items="${fileNmList}" varStatus="i">
							<c:if test="${i.index eq 0}">
								<a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
							</c:if>
							<c:if test="${i.index ne 0}">
								</br><a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
							</c:if>
						</c:forEach>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="sub_tit1">Contact Information</div>
		<table class="table2 table2_2 mab76">
			<colgroup>
				<col width="88" />
				<col width="177" />
				<col width="88" />
				<col width="*" />
				<col width="80" />
				
			</colgroup>
			<tbody>
				<tr>
					<th>Company</th>
					<td>${reqVo.corpo}<input type="hidden" name="corpo" value="${reqVo.corpo}"></td>
					<th>Name</th>
					<td>${reqVo.person}<input type="hidden" name="person" value="${reqVo.person}"></td>
					<td>Section ${reqVo.personType}<input type="hidden" name="personType" value="${reqVo.personType}"></td>
				</tr>
				</tr>
				<tr>
					<th>Phone</th>
					<td>${reqVo.phone}<input type="hidden" name="phone" value="${reqVo.phone}"></td>
					<th>e-mail</th>
					<td colspan="2">${reqVo.email}<input type="hidden" name="email" value="${reqVo.email}"></td>
				</tr> 
				<tr>
					<th>Company<br>profile</th>
					<td colspan="4">
						<c:set var="fileList" value="${fn:split(reqVo.file2,',')}" />
						<c:set var="fileNmList" value="${fn:split(reqVo.file2Nm,',')}" />
						<c:forEach var="vo" items="${fileList}" varStatus="i">
							<input type="hidden" name="file" value="${vo}">
						</c:forEach>
						<c:forEach var="vo" items="${fileNmList}" varStatus="i">
							<c:if test="${i.index eq 0}">
								<a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
							</c:if>
							<c:if test="${i.index ne 0}">
								</br><a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
							</c:if>
						</c:forEach>
					</td>
				</tr>
			</tbody> 
		</table>
		</form>
		<div class="sub_txt1 mab50">
			<p>ㆍAttached proposal document will be reviewed within 5 business days and will be used for review only
</p>
			<p>ㆍThe person in charge will make a response for the proposal
</p>
		</div>
		<div class="btn_area1">
			<a href="${pageContext.request.contextPath}/corporate/partnership.do" class="callPage">Cancle</a>
			<a id="btn_send">Submit</a>
		</div>
	</div>
