<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_01");
});
</script>

<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		VISION
	</div>
	<div class="sub_title2">
		FROM <strong><b>HABIT</b></strong> TO <strong><b>ECONOMIC</b></strong><span></span>
	</div>
	<div class="sub01_box1">
		<h1>Connected Car Service Provider, JastecM</h1>
		<p>
			JastecM provides services and products which enables connectivity for the Internet of Things (IoT). JastecM has connected many devices which have delivered reliable communication between human, systems, and devices. JastecM has been prepared for a safer place and prosperity of the future ‘Paradigm Shift’ through our services. 			
		</p>
	</div>
	<div class="img_box1"><img src="${pageContext.request.contextPath}/common/images/sub01_img1.jpg" alt="" /></div>
</div>