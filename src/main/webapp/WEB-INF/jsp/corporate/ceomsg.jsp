<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_03");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			CEO Message
		</div>
		<div class="sub_title7_2">
			JastecM will put as much effort as it could to become a leading IoT company<br>
			contributing to <span>people’s safety</span> in <span>ECO friendly method.</span> 
		</div>
		<div class="intro_box1">
			Merger of two companies created the next generation service<br>
			provider in the 4th Industrial Revolution with Jastec’s 25 years<br>
			experience in vehicle IT industry and InfinityPlus’s solution.<br>
		</div>
		<div class="intro_box2">
			<p>JastecM will provide a device that is competitive in price and functionally optimized to adapt IoT environment.
</p>
			<p>JastecM will provide a service that is safety oriented service with Fun & Wit through Connected Car and Self-Driving Car Service.
			</p>
			<p>Planning to develop Deep-Learning Intelligence Service that is based on accumulated data and technology. 
</p>
			<p>Aiming to reduce accident occurrence rate to Zero percent with Nudge function within the service.
</p>
			<p>Creating new value by connecting Smart devices from wearables to Biometric devices to our service. 
</p>
		</div>
		<div class="intro_box3">
			JastecM CEO<img src="${pageContext.request.contextPath}/common/images/sign.jpg" alt="" />
		</div>
	</div>