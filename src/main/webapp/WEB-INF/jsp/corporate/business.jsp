<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_04");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			Business scope & Strategy
		</div>
		<div class="sub_title7_2 mab140">
			The business scope of IoT market has shifted from<span>Device & Network</span><br>
			oriented service to Solution & Data. JastecM will offer a convergence<br>
			service between telematics and personal devices to lead the market trend.
			
		</div>
		<div class="sub02_box1 mab100">
			<div class="tit">
				JastecM’s<br>
				Based Data &<br>
				Technical Field
			</div>
			<p class="t1">
				JastecM already acquires technology of collecting and analyzing sensor and trip data through In-Vehicle Network and plans to develop a self-evolving service with a data set produced from our solution
			</p>
			<div class="sub02_box2">
				<span><img src="${pageContext.request.contextPath}/common/images/sub02_img1.jpg" alt="" />
					<strong>
						Acquired<br>
						specialized technology resources<br>
						in In-Vehicle Networks
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img2.jpg" alt="" />
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img3.jpg" alt="" />
					<strong>
						Patent Technology Based<br>
						ICT Converged Products & Solutions
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img2.jpg" alt="" />
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img4.jpg" alt="" />
					<strong>Use of various Public Data<br>
					Set for deeper study and<br>
					service improvement</strong>
				</span>
			</div>
		</div>
		<div class="sub02_box1 mab100">
			<div class="tit">
				Market<br>
				Demand<br>
				Business
			</div>
			<p class="t1">
				Universal telematics service that assists driver in safe and economic and<br>
				entertaining manner in low cost  
			</p>
			<ul class="sub02_box3">
				<li>
					<p>Demand of Universal<br>Telematics Service</p>
					<p>Expand Vehicle-ICT<br>
					convergence Infrastructure<br>
					and platform</p>
					<p>Public understanding of<br>
					traffic accident safeguard<br>
					necessity</p>
				</li>
				<li>
					<p>Demand for driver’s aid<br>
					and  maintenance service<br>
					through Smartphone</p>
					<p>Open platform for various<br>
					business</p>
					<p>Cooperative ITS<br>
					development through V2X<br>
					technology</p>
				</li>
			</ul>
			<ul class="sub02_box4">
				<span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img5.jpg" alt="" />
					<strong>
					Standardized<br>Open Platform
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img5.jpg" alt="" />
					<strong>
						Expand<br>Connected Car<br>Compatible Car<br>List
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img5.jpg" alt="" />
					<strong>
							Driver`s<br>Assistant/Economic/Safety<br>Improvement
					</strong>
				</span><span>
					<img src="${pageContext.request.contextPath}/common/images/sub02_img5.jpg" alt="" />
					<strong>
					Convergent<br>Business with<br>various<br>industries
					</strong>
				</span>
			</ul>
		</div>
		<div class="sub02_box1">
			<div class="tit">
				Technological<br>
				Developing<br>
				Strategy
			</div>
			<p class="t1">
				JastecM will lead the connected car industry with reliable and trustworthy<br>
				products and solution
			</p>
			<img src="${pageContext.request.contextPath}/common/images/sub02_img6.jpg" alt="" />
		</div>

	</div>