<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_06");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			Certifications
		</div>
		<div class="sub_title6">
			<span>Technology<br>Certifications</span><strong></strong>
		</div>
		<div class="sub02_box6">
			<div class="tit">
				<p>U.S. FCC Certifications</p>
				<p>KIDI Certification<br>(Korea Insurance<br>Development Institute)
</p>
				<p>KCC Certification</p>
			</div>
			<table class="table3">
				<colgroup>
					<col width="178">
					<col width="151">
					<col width="*">
				</colgroup>
				<thead>
					<tr>
						<th>Certification</th>
						<th>Model</th>
						<th>Certification No.</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>FCC</th>
						<th>JTBT-1100</th>
						<th>UK4JTBT1100</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JTBT-2100</th>
						<th>UK4JTBT2100</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JTWF-1100</th>
						<th>UK4JTWF1100</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JTCM-1000</th>
						<th>UK4JTCM1000</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JTGM-1100</th>
						<th>UK4JTGM-1100</th>
					</tr>
					<tr>
						<th>FCC</th>
						<th>JFCM-1000</th>
						<th>KCC-CCM-JS0-JTBT1100</th>
					</tr>
					<tr>
						<th>KC</th>
						<th>JTBT-1100</th>
						<th>KCC-CMM-JS0-JTBT2100</th>
					</tr>
					<tr>
						<th>KC</th>
						<th>JTBT-2100</th>
						<th>KCC-CCM-JS0-JTWF1100</th>
					</tr>
					<tr>
						<th>보험개발원</th>
						<th>JTBT-2100</th>
						<th>KIDI-11-07</th>
					</tr>
					<tr>
						<th>보험개발원</th>
						<th>JTUB-1000</th>
						<th>KIDI-11-10 (대전시청 요일제)</th>
					</tr>
				</tbody>
			</table>
			<img src="${pageContext.request.contextPath}/common/images/sub02_img9.jpg" alt="" class="mab97" />
		</div>
		<div class="sub_title6">
			<span>Intellectual<br>Properties</span><strong></strong>
		</div>
		<div class="sub02_box6">
			<div class="tit">
				<p class="p1">
					JastecM’s Patent Based<br>
e-Call ISO in progress
				</p>
				<p class="p1">
					Usage Based Insurance<br>System Publicness
				</p>
				<p>Vehicle Sensor/Trip<br> Data Source<br> Technology</p>
			</div>
			<table class="table3 table3_2">
				<colgroup> 
					<col width="105">
					<col width="160">
					<col width="*">
				</colgroup>
				<thead>
					<tr>
						<th>classification</th>
						<th>Property No.</th>
						<th>Title</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>Tradmark</th>
						<th>4500481620000</th>
						<td>ViewCAR</td>
					</tr>
					<tr>
						<th>Patent</th>
						<th>10-1382498</th>
						<td>Airbag  Deployment Detection System and Process</td>
					</tr>
					<tr>
						<th>Patent</th>
						<th> 10-1618118</th>
						<td>
							BCM Control System & Process through external device
							</td>
					</tr>
					<tr>
						<th>patent</th>
						<th>10-2016-0028454</th>
						<td>
							BCM Control Unit and Vehicle Control System
						</td>
					</tr>
					<tr>
						<th>Intellectual<br>Property</th>
						<th>C-2014-007978</th>
						<td>ViewCAR</td>
					</tr>
					<tr>
						<th>Intellectual<br>Property</th>
						<th>C-2016-008356</th>
						<td>
							VDAS(Vehicle Driving Analysis System
						</td>
					</tr>
					<tr>
						<th>ISO</th>
						<th>ISO/WD20530</th>
						<td>
							Intelligent Transport Systems — Information for<br>
							emergency service support via<br>
							Personal ITS station —<br>
							General requirements and technical definition
						</td>
					</tr>
					<tr>
						<th>Certification</th>
						<th>881</th>
						<td>Location Based Service Certification</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>