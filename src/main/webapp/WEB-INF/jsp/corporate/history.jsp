<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_02");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			History
		</div>
		<div class="sub_title7_2">
			Vehicle IT Development Expert, Jastec, and IoT Solution Expert, InfinityPlus,<br>
			<b>have merged and become a Connectivity Service Provider <span>JastecM</span>
			</b>
		</div>
		<div class="history_area">
			<h1><img src="${pageContext.request.contextPath}/common/images/his_img1.jpg" alt="" /></h1>
			<div class="history_area2">
				<div class="history_area2_1">
					<ul>
						<li>
							<h2><img src="${pageContext.request.contextPath}/common/images/his_img2.jpg" alt="" /></h2>
							<h3>InfinityPlus Co.,ltd</h3>
						</li>
						<li>
						    <p>Developed and Commercialized Safe& Economic Driving index based on TAAS from KATRI
</p>						
							<p>Participated in Contents and Platform establishment of K-Global Smart, Mobile Star Corporate Vehicle-ICT  convergence  from MSIP(Ministry of Science, ICT and Future Planning)
</p>
							<p>Standardized e-Call on  2015 Standardization Framework of KATS(Korean Agency for Technology and Standards)
</p>
							<p>Developed LG Elec. LGU+ oneM2M based Solution
</p>
							<p>Signed UBI Service development MOU with Hyundai Insurance.
</p>
							<p>International Standardization in progress  based on own patent ISO/NP20530
</p>
							<p>Commercialized Connected Car Service  ‘ViewCAR’
</p>
							<p>Selected as a recipient company of KISTA(Korea Intellectual Property Strategy Agency) in  ‘Smart Car field IP’
</p>
							<p>Developed  the world’s  first  ACN  Solution
</p>
							<p>Developed KORAIL Pass Reservation Portal websites 
</p>
							<p>Established in Nov. 2006
</p>						
							</li>
					</ul>
				</div>
				<div class="history_area2_2">
					<ul class="mab258">
						<li>
							<h3>JastecM</h3>
						</li>
						<li>
							<p>July 2016, Jastec Co.,ltd. & InfinityPlus M&A
</p>
						</li>
					</ul>
					<ul>
						<li>
							<h2><img src="${pageContext.request.contextPath}/common/images/his_img3.jpg" alt="" /></h2>
							<h3>Jastec Co.,ltd</h3>
						</li>
						<li>
							
							<p>Participated in NIA Flagship Project
</p>
							<p>Supplied IoT OBD for KT UBI
</p>
							<p>OBD II Device Provider of Audiovox/Airtyme/Modus
</p>
							<p>Supplied OBD to SAMSUNG Elec. for Service vehicles FMS service
</p>
							<p>Supplied von-STD for UBI to DELPHI Australia
</p>
							<p>SAMSUNG Elec. named Jastec as OBD II partner for Galaxy Tab project for Toyota Korea
</p>
							<p>City of DaeJeon has named Jastec as OBD II- PAYD Operator
</p>
							<p>Rental Car OBD II was developed for KT/Kumho Rental Car Co. (Largest Rental Car Co. in Korea)
</p>
							<p>2nd,3rd Gen Vehicle Scan Tool Developer & Provider
</p>
							<p class="mab0">Established in May 1990
</p>
						</li>
					</ul>
					<span class="s1"></span>
					<span class="s2"></span>
					<span class="s3"></span>
				</div>
			</div>
		</div>
	</div>