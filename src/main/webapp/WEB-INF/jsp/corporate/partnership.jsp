<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script src='${pageContext.request.contextPath}/common/js/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02"); 
	initMenuSel("LM02_08");
	
	$('#frm').ajaxForm({
		beforeSubmit: function (data,form,option) {
			return true;
		},
		success: function(res,status){
	    	
			var rtvCd = res.rtvCd;
			if(rtvCd == "0"){
				alert("error");
			}else{
				var url = "/index.do";
				res.rtvMap.callPage = "/corporate/partnershipReceipt";
				$JBIZ.instance_post(url,res.rtvMap);
			}
		},
		error: function(){
	    	alert("error");
		}                               
	});
	
	$("#frm").submit(function(){return false});
	
	$("#btn_send").on("click",function(){
		if(!($("#input-1").is(":checked"))){ 
			alert("Please agree our demands ");
			return false;
		}
		$('#frm').submit();
	});
});
</script>

	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			Cooperation Proposals
		</div>
		<div class="sub_txt2 mab18">JastecM is always opened for Win-Win strategic partnership.
</div>
		<div class="sub06_box1"> 
			<p>For Inquiry, please contact with following e-mail or phone number.
</p>
			<p>Attached proposal document will be reviewed within 5 business days and will be used for review only.
</p>
			<p>The person in charge will make a response for the proposal.
</p>
		</div>
		<form id="frm" action="${pageContext.request.contextPath}/com/fileUpload.do" method="post" enctype="multipart/form-data">
			<div class="sub_tit1">Proposal</div>
			<table class="table2 table2_3 mab76">
				<colgroup>
					<col width="73" />
					<col width="*" />
					<col width="260" />
				</colgroup>
				<tbody>
					<tr>
						<th>Title<br>(Required)
						</th>
						<td><input type="text" class="w456" name="name" placeholder="Title" /></td>
						<td>
							<div class="select_type1">
								<div class="select_type1_1">
									<select name="partnershipType" class="sel">
										<option value="">Classification</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th>Content<br>
(Required)
						</th>
						<td colspan="2">
							<textarea name="contents"></textarea>
							<p class="t1">* Please avoid to put core technology contents that may violate intellectual Property Right.
							</p>
						</td>
					</tr>
					<tr>
						<th>File</th>
						<td colspan="2">
							<div class="btn_file"><input type="file" name="file" class="multi" maxlength="3"/></div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="sub_tit1">Contact Information</div>
			<table class="table2 table2_3">
				<colgroup>
					<col width="73" />
					<col width="*" />
					<col width="59" />
					<col width="200" />
					<col width="260" />
				</colgroup>
				<tbody>
					<tr>
						<th>Company</th>
						<td><input type="text" class="w188" name="corpo" placeholder="Please input company" /></td>
						<th>Name</th>
						<td><input type="text" class="w188" name="person" placeholder="Please input name" /></td>
						<td>
							<div class="select_type1">
								<div class="select_type1_1">
									<select name="personType" class="sel">
										<option value="">Classification</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="table2 table2_3">
				<colgroup>
					<col width="73" />
					<col width="*" />
					<col width="71" />
					<col width="304" />
				</colgroup>
				<tbody>
					<tr>
						<th>Phone</th>
						<td><input type="text" class="w304" name="phone" placeholder="Please input phone number" /></td>
						<th>e-mail</th>
						<td><input type="text" class="w304" name="email" placeholder="Please input e-mail" /></td>
					</tr>
				</tbody>
			</table>
			<table class="table2 table2_3 mab76">
				<colgroup>
					<col width="73" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>Company Profile
</th>
						<td>
							<div class="file_txt">document file</div>
							<div class="btn_file"><input type="file" name="file2" class="multi" accept="" maxlength="3"/></div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="sub_tit1">Consent to Collect, Use and Disclose Personal Information</div>
			<div class="sub06_box2">
				The personal information provided within this application is collected by JastecM.
				<br>
				1. Collecting Personal Information Items : Company, Name, e-mail, Phone
				<br>
				2. Purpose :  To confirm the identification of proposer and to make a contact
<br>
				3. Retention Period :  PI will be terminated after a month
<br>
				4. Right to refuse consent : Proposer may refuse to consent and restricted to<br>
				make a proposal JastecM complies with Personal Information Protection Policy.

			</div>
			<div class="agree_box demo-list mab63">
				<input tabindex="1" type="checkbox" id="input-1" class="ichk"><label for="input-1">Yes, I have read and I agree
</label>
			</div>
		</form>
		<div class="btn_area1">
			<a href="${pageContext.request.contextPath}/corporate/partnership.do" class="callPage">Cancle</a>
			<a id="btn_send">Submit</a>
		</div>
		<br><br><br><br><br><br><br>
		<div class="sub05_box3">
			<h1>Partnership Department</h1>
			<ul>				
				<li>
					<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub05_img8.jpg" alt=""></span>
					<h2>Partnership / Service Inquiry
					</h2>
					<div>
						<span class="w209">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="">Youngmin Park (Strategic Marketing/Manager)

						</span><span class="w146">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="">02-452-9710
						</span><span>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="">youngmin.park@jastecm.com
						</span>
					</div>
				</li>
				<li>
					<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img9.jpg" alt=""></span>
					<h2>Overseas / Other Inquiry
</h2>
					<div>
						<span class="w209">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="">James Jang (Strategic Marketing/Assistant Manager)

						</span><span class="w146">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="">031-8060-0321
						</span><span>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="">jasmes.jang@jastecm.com
						</span>
					</div>
				</li>
			</ul>
		</div>
	</div>