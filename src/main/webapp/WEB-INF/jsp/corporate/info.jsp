<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM02");
	initMenuSel("LM02_05");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			JastecM
		</div>
		<div class="sub_title7_2">
			JastecM is <span>IoT Total Solution & Service</span> Provider who has vehicle ICT products &<br>
			solutions
		</div>
		<div class="sub01_box1 mab35">
			<p>
				JastecM promise to provide a safe and<br>
				well-suited smart driving service to everyone
			</p>
		</div>
		<div class="movie_area">
			<iframe width="800" height="452" src="https://www.youtube.com/embed/3zJlbkJMSjw" frameborder="0" allowfullscreen></iframe>
		</div>
		<div class="sub02_box5 mab75">
			<h1>Corporate Overview Download 
			</h1>
			<p>JastecM corporate overview in detail
			</p>
			<div class="btn">
				<a href="${pageContext.request.contextPath}/common/ref/JASTECM_Corporate Overview(B2B_ver1.1).pdf" target="_blank">KOREAN</a><a href="${pageContext.request.contextPath}/common/ref/JASTECM_Cooperative Overview(B2B_ver1.1_Eng).pdf" target="_blank">ENGLISH</a>
			</div>
		</div>
		<div class="sub02_box5 mab75">
			<h1>CI Download</h1>
			<p>Vehicle IT Corporate Jastec Logo merged with M from ‘Mobile’ 
			</p>
			<img src="${pageContext.request.contextPath}/common/images/sub02_img7.jpg" alt="" class="mab14" />
			<div class="btn">
				<a href="${pageContext.request.contextPath}/common/ref/jastecM_logo.jpg" target="_blank">JPG</a><a href="${pageContext.request.contextPath}/common/ref/jastecM_ai.ai" target="_blank">AI</a>
			</div>
		</div>
		<div class="sub02_box5">
			<h1>Advertising Slogan
			</h1>
			<p>
				‘Paradigm Shift’ for the living of safer and well-suited habits<br>
‘Shift in Thought’ in the 4th Scientific Revolution with JastecM 

			</p>
			<img src="${pageContext.request.contextPath}/common/images/sub02_img8.jpg" alt="" />
		</div>
	</div>