<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM04");
	initMenuSel("LM04_02");
});

$(document).ready(function(){
    $(".tabbable").find(".tab").hide();
    $(".tabbable").find(".tab").first().show();
    $(".tabbable").find(".tabs li").first().find("a").addClass("active");
    $(".tabbable").find(".tabs").find("a").click(function(){
        tab = $(this).attr("href");
        $(".tabbable").find(".tab").hide();
        $(".tabbable").find(".tabs").find("a").removeClass("active");
        $(tab).show();
        $(this).addClass("active");
        return false;
    });
});

</script>
	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			Automotive IoT Device
		</div>
		<div class="sub03_img3">
			<img src="${pageContext.request.contextPath}/common/images/sub03_img3.jpg" alt="" />
		</div> 
		
		
		<div class="tabbable">
    <ul class="tabs">
    <div class="sub_tab">
			<a href="#On-board형" class="on">On-Board</a><a href="#On-Line형">On-Line</a>
		</div>
    	
    </ul>
    <div class="tabcontent">
        <div id="On-board형" class="tab">
            <ul class="sub03_box2">
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img11.jpg" alt="" /></div>
						<h1>Model : JTBT-2200 / JTWF-1100 (Von-STD)</h1>
						<p>Wifi, Bluetooth capabilities</p>
						<p>
							Collects Vehicle Sensor, Driving trip data<br>
						    - Trip/Daily/Monthly Data record, Driving Behavior Analysis, Diagnosis, Consumables Management
						</p>
						<p>
							Economic Driving Habit Coaching<br>
							- Gas Mileage, CO2 emission, Rapid Acceleration/Deceleration, Sudden Stop Analysis 
						</p>
						<p>
							PAYD/Mileage Insurance service support<br>
						</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>Features</b></dt>
								<dd class="d2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UBI, e-Call, FMS service</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Processor</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;32-bit ARM Cortex-M3(256KB Flash, 64KB SRAM)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Protocol</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;
									ISO 9141-2, ISO 14230-4(KWP-2000),<br>&nbsp;&nbsp;&nbsp;&nbsp;ISO 15765-4(CAN),SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonSTD" class="callPage">Specification</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img12.jpg" alt="" /></div>
						<h1>Model : JTBT-3100 (Von-G)</h1>
						<p>
							von-STD with G-Sensor<br>
							- Detecting collision impact and Sharp Turn for e-Call & UBI service
						</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>Features</b></dt>
								<dd class="d2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UBI, e-Call, FMS service</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Processor</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;32-bit ARM Cortex-M3(256KB Flash, 64KB SRAM)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Protocol</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;
									ISO 9141-2, ISO 14230-4(KWP-2000),<br>&nbsp;&nbsp;&nbsp;&nbsp;ISO 15765-4(CAN),SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonG" class="callPage">Specification</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img13.jpg" alt="" /></div>
						<h1>Model : JTBT-4100 (Von-G2)</h1>
						<p>
							G-Sensor & GPS embedded von-STD<br>
							- Collects location record and transmit location data in case of collision<br>
							- Advanced sharp turn detecting accuracy, 12/24 support
						</p>
						<p>Advanced FMS, e-Call service available</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>Features</b></dt>
								<dd class="d2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UBI, e-Call, FMS service</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Processor</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;32-bit ARM Cortex-M3(256KB Flash, 64KB SRAM)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Protocol</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;
									ISO 9141-2, ISO 14230-4(KWP-2000),<br>&nbsp;&nbsp;&nbsp;&nbsp;ISO 15765-4(CAN),SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonG2" class="callPage">Specification</a></div>
					</li>					
				</ul>
			</div>
        </div>
        <div id="On-Line형" class="tab">
            <ul class="sub03_box2">
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img4.jpg" alt="" /></div>
						<h1>Von-S2</h1>
						<p>Air Update</p>
						<p>Collects Sensor ·Trip Data</p>
						<p>OBD II Support</p>
						<p>Vehicle Management/ Location Monitoring/ Driving Behavior Analysis</p>
						<p>Digital I/O Extension</p>
						<p>Location based Emergency Rescue/ Theft Tracking Service</p>
						<p>For overseas service</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>Features</b></dt>
								<dd class="d2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For overseas service</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Processor</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;32-bit ARM Cortex-M3 (4M Memory)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Protocol</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;
									ISO 9141-2, ISO 14230-4(KWP-2000),<br>&nbsp;&nbsp;&nbsp;&nbsp;ISO 15765-4(CAN),SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonS2" class="callPage">Specification</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img5.jpg" alt="" /></div>
						<h1>Von-S21</h1>
						<p>Air Update
						<p>Collects Sensor ·Trip Data
						<p>OBDII Support
						<p>Vehicle Management/ Location Monitoring/ Driving Behavior Analysis
						<p>Digital I/O extension
						<p>Location based emergency/ Theft tracking service</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>Features</b></dt>
								<dd class="d2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Domestic service device</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Processor</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;32-bit ARM Cortex-M3 (8M Memory)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Protocol</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;
									ISO 9141-2, ISO 14230-4(KWP-2000),<br>&nbsp;&nbsp;&nbsp;&nbsp;ISO 15765-4(CAN),SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonS21" class="callPage">상세정보</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img6.jpg" alt="" /></div>
						<h1>Von-S3</h1>
						<p>KT(Heungkuk,Meritz) insuranse UBI demonstration project device </p>
						<p>Acquired IoT certification from domestic mobile service provider</p>
						<p>Support 12/24 Volts</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>Features</b></dt>
								<dd class="d2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Domestic service device</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Processor</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;32-bit ARM Cortex-M3 (8M Memory)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Protocol</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;
									ISO 9141-2, ISO 14230-4(KWP-2000),<br>&nbsp;&nbsp;&nbsp;&nbsp;ISO 15765-4(CAN),SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonS3" class="callPage">Specification</a></div>
					</li>
					<li>
						<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img7.jpg" alt="" /></div>
						<h1>Von-F</h1>
						<p>Supports J1939/J1708</p>
						<p>Vehicle Management/ Location Monitoring</p>
						<p>Trip record/ Trip analysis</p>
						<p>Digital I/O extension</p>
						<p>Emergency service/ Theft tracking service</p>
						<p>Supports 12/24 Volt</p>
						<div class="txt">
							<dl>
								<dt class="d1"><b>Features</b></dt>
								<dd class="d2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DTG(Digital Tachograph)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Processor</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;32-bit ARM Cortex-M3 (8M Memory)</dd>
							</dl>
							<dl>
								<dt class="d3"><b>Protocol</b></dt>
								<dd class="d4">&nbsp;&nbsp;&nbsp;
									ISO 9141-2, ISO 14230-4(KWP-2000),<br>&nbsp;&nbsp;&nbsp;&nbsp;ISO 15765-4(CAN),SAE J1850(PWM/VPWM)
								</dd>
							</dl>
						</div>
						<div class="btn"><a data-page="/product/VonF" class="callPage">Specification</a></div>
					</li>
				</ul>
			</div>
        </div>
      
   	
			<br><br><br><br><br>
			<div class="sub05_box3">
				<h1>Purchase Inquiry</h1>
				<ul>
					<li>
					<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub05_img8.jpg" alt="" /></span>
					<h2>Solution Purchase Inquiry</h2>
					<div>
						<span class="w209">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="" />Youngmin.Park<br>(Manager)
						</span><span class="w146">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="" />02-452-9710
						</span><span>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="" />youngmin.park@jastecm.com
						</span>
					</div>
				</li>
					<li>
						<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img9.jpg" alt=""></span>
							<h2>Device Purchase Inquiry</h2>
						<div>
							<span class="w209">
								<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="">James Jang<br>(Assistant Manager)
							</span><span class="w146">
								<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="">031-8060-0321
							</span><span>
								<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="">jasmes.jang@jastecm.com
							</span>
						</div>
					</li>
				</ul>
			</div>
		</div>