<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
</script>
<div class="sub_layout">
		<div class="sub_title5 mab68">
			상세정보
		</div>
		<div class="sub_tit1">기본 정보</div>
		<div class="sub03_box3">
			<div class="img"><img src="${pageContext.request.contextPath}/common/images/sub03_img7.jpg" alt="" /></div>
			<h1>von-F</h1>
			<p>대형트럭, 버스, 법인차량   등 상용 차량을 효율적으로 관리하기 위한 특화된 솔루션입니다.</p>
		</div>
		<div class="sub_tit1">제품 사양</div>
		<ul class="sub03_box4">
			<li>
				<span>프로세스(Processor)</span>32-bit ARM Cortex-M3(512KB Flash, 128KB SRAM)

			</li>
			<li>
				<span>메모리(Memory)</span>8MB Serial Flash Memory
			</li>
			<li>
				<span>상태 표시(Status)</span>4-LEDs(Power, Status, Modem, GPS)

			</li>
			<li>
				<span>차량통신</span>SAE J1939, SAE1708(J1587), ISO9141-2, ISO15765-4, ISO 14230-4,<br> SAE J1850(PWM/VPW)

			</li>
			<li>
				<span>차량 Data Links</span>SAE J1939 : Battery, Backup Battery, Ignition, CAN High, CAN Low<br>
					SAE J1708 : Battery, Backup Battery, Ignition, J1708+, J1708-
			</li>
			<li>
				<span>RS-232</span>1 Port(TX, RX & GND), 115,200BPS

			</li>
			<li>
				<span>Analog/Digital Input</span>6 Ports
			</li>
			<li>
				<span>Relay Drive Output</span>4 Ports (Max 0.5A/24V)
			</li>
			<li>
				<span>Reference Drive Output</span>1 Ports (Max 0.5A/5V)
			</li>
			<li>
				<span>GSM Band</span>GPRS/EDGE 850,900,1800,1900MHz
			</li>
			<li>
				<span>UMTS</span>HSPA+ 800/850,900,2100MHz

			</li>
			<li>
				<span>GPS</span>A-GPS Support
			</li>
			<li>
				<span>G-Sensor</span>Bosch 3-Axis 10-Bit Resolution, BMA150
			</li>
			
			<li>
				<span>Wireless(Optional)</span>WiFi 802.11b/g/n (Optional) 
			</li>
			<li>
				<span>USB Port</span>USB 2.0 Compatible, VCP(Vitural COM Port) Supported
			</li>
			<li>
				<span>전압</span>DC 8V ~ 36V, Sleep: ≤200mW(3G기준)
			</li>
			
			<li>
				<span>크기(Dimensions)</span>Max. 175mm(W) x 78mm(D) x 39mm(H)
			</li>
			<li>
				<span>무게(Weight)</span>180g

			</li>
			<li>
				<span>동작온도</span>-25 ~ +70 ℃
				
			</li>
			<li>
				<span>보관온도</span>-40 ~ +80 ℃
			</li>
		</ul>
		<div class="sub_tit1">제공 서비스</div>
		<div class="sub03_box5">
			<ul class="sub03_box4">
				<li>
					<span>차량고장진단<br>(Vehicle Diagnostics)</span>
					ECU 결함코드 점검<br>
					ECU 결함코드 (DTC) 소거<br>
					Emissions 관련 항목 점검<br>
					냉각수 온도 상시 점검<br>
					배터리/발전기 상태 점검<br>
					운행정보 점검
				</li>
			</ul>
			<ul class="sub03_box4">
				<li>
					<span>안전 경제운전<br>(Safety & Eco Driving)</span>
					급가감속 정보 알림 서비스<br>
					엔진성능 모니터링<br>
					운행시작 / 종료일시<br>
					운행거리 , 운행정보<br>
					운행 요약 정보<br>
					소모품 교환 주기 관리<br>
					위치기반서비스(Location Based Service)<br>
					운전성향분석<br>
					차량 Door Lock/Unlock<br>
					안전벨트/브레이크 신호 모니터링<br>
					Digital Input/Output Supported(Input:6, Output:4) 


				</li>
			</ul>
		</div>
		<div class="sub03_box6">
			<a href="#"><img src="${pageContext.request.contextPath}/common/images/btn1.jpg" alt="" /></a>
		</div>
		<div class="bo_view_btn">
			<a href="#"><img src="${pageContext.request.contextPath}/common/images/prev3.jpg" alt=""></a><a href="#"><img src="${pageContext.request.contextPath}/common/images/next3.jpg" alt=""></a>
			<span><a data-page="/product/type" class="callPage">목록</a></span>
		</div>
	</div>