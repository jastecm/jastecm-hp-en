<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM04");
	initMenuSel("LM04_01");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			Specialized Technology
		</div>
		<div class="sub_title7_2">
		<span>Global Vehicle IoT Products</span> with JastecM’s advanced technology 
		</div>
		<ul class="sub03_box1">
			<li>
				<span>MQTT<br>Establishment</span>
				ㆍ Light weight Protocol for M2M & IoT: Suitability↑<br>
				ㆍ Low Power·Low Bandwidth Service<br>
				ㆍ Suitable for IoT & Convergence Service and Emergency Service such as e-Call<br>
				ㆍ Establish Intelligent Network Technology<br>
				ㆍ Optimized Data Process in Real-Time<br>
				ㆍ Flexible Service is possible with embedded specialized Program: O2O Market Expansion ↑
				<div><img src="${pageContext.request.contextPath}/common/images/sub03_img1.jpg" alt="" /></div>
				<br><br><br><br>
			</li>
			<li>
			<span>Flexible<br>Business with<br>Module Type<br>OBD</span>
				ㆍ Various Services are available to meet partner’s needs with prompt development in low cost.<br>
				ㆍ Appropriate Sensors applied by business type.<br>
				ㆍ Supports both LTE-MTC (KT/LG U+) & LoRA (SKT) network modules<br>
				&nbsp;&nbsp;&nbsp;- Long range Communication in Low Power·Low Bandwidth<br>
				&nbsp;&nbsp;&nbsp;- Low Initial Setup Cost<br>
				&nbsp;&nbsp;&nbsp;- Various Services are available by partnering with Alliance companies by Module Manufactures
				<div><img src="${pageContext.request.contextPath}/common/images/sub03_img2.jpg" alt="" /></div>
			</li>
		</ul>
		
	</div>