<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_06");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			V Scoring
		</div>
		<div class="sub_title7_2">
			UBI(Usage Based Insurance) Solution. V Scoring
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img48.jpg" alt="" /></div>
		<div class="sub_tit1">Service Outline</div>
		<div class="sub05_box6">
			<p>Premium discount rates is determined by analysis of trip data which is collected through OBD and smartphone. V Scoring is a solution for<br>calculating driving behavior score by collecting/analysis/process trip data.</p>
			<p>Co-developed KoROAD’s TAAS based UBI analysis solution</p>
		</div>
		<br><br><br><br><br>
		<div class="sub_tit1">Premium Determination factors and Discount Rate</div>
		<img style="margin-left:120px;" src="${pageContext.request.contextPath}/common/images/sub05_img49.jpg" alt="" />
		<div class="sub05_box6">
			<p>In U.S. and G.B., UBI Policy holders rate reached 8.4%(As of 2014) and already entered mass market.</p>
			<p>Progressive achieved USD 1 billion sales record in 2012, starting as mileage based insurance in 1998.</p>
			<p>25% of Car insurance policy holders will change to UBI by 2020, and USD 30 billion annual growth expected, a joint product between<br>insurance company and mobile network provider.</p>
		</div>
		<br><br><br>
		<table class="table3">
			<colgroup>
				<col width="182" />
				<col width="*" />
				<col width="229" />
			</colgroup>
			<thead>
				<tr>
					<th>Insurance</th>
					<th>Determinants
					</th>
					<th>Discount</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Progressive</td>
					<td>Sudden Brake, Trip Distance, Driving Hours</td>
					<td>Max(30%)</td>
				</tr>
				<tr>
					<td>State Farm</td>
					<td>Trip Distance, Sharp turns, Sudden Acceleration, Sudden Brake, Speeding, Driving Hours</td>
					<td>The first(5%), Max(50%)</td>
				</tr>
				<tr>
					<td>Allstate</td>
					<td>Sudden Brake, Speeding, Trip Distance, Driving Hours</td>
					<td>The first(10%), Max(30%)</td>
				</tr>
				<tr>
					<td>Hartford</td>
					<td>Driving Location, Driving Hours, Speeding, Sudden Acceleration, Sudden Brake</td>
					<td>The first(5%), Max(25%)</td>
				</tr>
			</tbody>
		</table>
		<br><br><br><br><br><br>
		<div class="sub_tit1">V Scoring Analysis Process and Scoring Types</div>
		<img src="${pageContext.request.contextPath}/common/images/sub05_img50.jpg" alt="" />
	</div>