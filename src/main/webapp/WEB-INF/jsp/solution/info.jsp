<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_01");
});
</script>
	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			SOLUTION
		</div>
		<div class="sub_title7_2">
		<b>JastecM’s devices and service turn your car into a <span>connected car</span> that can<br>collect/analysis/process the sensor, trip, trouble code data through<br>wireless network.</b>
		</div>
		<ul class="sub05_box1">
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img1.jpg" alt="" />
				<h1>Connected Car Solution, A reasonable offer of<br>In-Vehicle Infotainment and management!</h1>
				<p>The era of 45 Millions smartphone users, 20 Million Registered Vehicles!!
Automotive-ICE Convergence Service becomes Must-do of the era.</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img2.jpg" alt="" />
				<h1>Smart Driving in your hand,<br>
All set to hit the road with OBD installation~</h1>
				<p>Handy vehicle management and driving behavior coaching service<br>available with Automotive-ICT Device and Smartphone application!</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img3.jpg" alt="" />
				<h1>Automotive-ICT Convergence Service,<br>Enables Communicating between Smartphone and vehicle</h1>
				<p>Communicates with Deep Learning A.I vehicle platform<br>and provides safe and handy solution.</p>
			</li>
		</ul>
		<div class="sub_title6 mab13">
			<span>Solution Shortcut</span><strong></strong>
		</div>
		<ul class="sub05_box2">
			<li>
				<h1>Vehicle Management Smartphone App Solution, VSAPP</h1>
				<p>Driving Behavior Monitoring Service and Vehicle Management<br>
Service on Smartphone.</p>
				<a data-page="/solution/apps" class="callPage">Shortcut</a>
			</li>
			<li>
				<h1>FMS(Fleet Management System)Solution, VDAS</h1>
				<p>FMS Solution with a trip record service for commercial vehicle’s tax<br>deduction and Location Monitoring Service.</p>
				<a data-page="/solution/vdas" class="callPage">바로가기</a>
			</li>
			<li>
				<h1>Vehicle Maintenance Management Solution, VClinic</h1>
				<p>Web based Vehicle Maintenance Management Solution for Automotive<br>
IoT device owner as well as any driver</p>
				<a data-page="/solution/clinic" class="callPage">바로가기</a>
			</li>
			<li>
				<h1>Car-Sharing Solution, V Share</h1>
				<p>Establish Car-Sharing Solution with OBD II installation</p>
				<a data-page="/solution/share" class="callPage">Shortcut</a>
			</li>
			<li>
				<h1>UBI(Usage Based Insurance) Solution, VScoring</h1>
				<p>Calcurate premium discount rate through the analysis of collected data<br>from either OBDII or smartphone</p>
				<a data-page="/solution/scoring" class="callPage">Shortcut</a>
			</li>
			<li>
				<h1>e-Call(Emergency Call) Solution, VCall</h1>
				<p>Provides an emergency call service in case of serious accident that<br>can be triggered either automatically or manually with our IoT device<br>and solution. The solution is fitted to Aftermarket e-Call service.</p>
				<a data-page="/solution/call" class="callPage">Shortcut</a>
			</li>
			<li>
				<h1>V2X Based Solution, VCOMM</h1>
				<p>Preventing accident through WAVE(Wireless Access in Vehicular<br>Environment) which is IEEE technological standard.</p>
				<a data-page="/solution/comm" class="callPage">Shortcut</a>
			</li>
		</ul>
		<div class="sub05_box3">
			<h1>Purchase Inquiry</h1>
			<ul>
				<li>
					<span class="img"><img src="${pageContext.request.contextPath}/common/images/sub05_img8.jpg" alt="" /></span>
					<h2>Solution Purchase Inquiry</h2>
					<div>
						<span class="w209">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img5.jpg" alt="" />Youngmin.Park(Manager)
						</span><span class="w146">
							<img src="${pageContext.request.contextPath}/common/images/sub05_img6.jpg" alt="" />02-452-9710
						</span><span>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img7.jpg" alt="" />youngmin.park@jastecm.com
						</span>
					</div>
				</li>
			</ul>
		</div>
	</div>
	