<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_04");
});


$(document).ready(function(){
	$(".toggleMenu").on("click",function(){
		$(".toggleMenu").removeClass("active");
		$(this).addClass('active');
		$(".tabbable").find(".tab").hide();
		tab = $(this).data("menu");
		
		$(tab).show();
		
	});
	
	$(".toggleMenu").eq(0).trigger("click");
	
});
</script>
	
<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		VClinic
	</div>
	<div class="sub_title7_2">
		Vehicle Maintenance Solution, VClinic
	</div>
	<div><img src="${pageContext.request.contextPath}/common/images/sub05_img30.jpg" alt="" /></div>
	
	<div class="sub_tit1">Service Details</div>
		
	<div class="tabbable"> 
		<ul class="tabs">
			<div class="sub_tab">
				<a data-menu="#menu1" class="toggleMenu" style="width: 112px">Registration</a><a data-menu="#menu2" class="toggleMenu" style="width: 112px">Account</a><a data-menu="#menu3" class="toggleMenu" style="width: 112px">Customer</a><a data-menu="#menu4" class="toggleMenu" style="width: 112px">Maintenance</a><a data-menu="#menu5" class="toggleMenu" style="width: 112px">Reservation</a><a data-menu="#menu6" class="toggleMenu" style="width: 112px">Message</a><a data-menu="#menu7" class="toggleMenu" style="width: 112px">Community</a>
			</div>
		</ul> 
		<div class="sub05_box7">
			<div class="tabcontent">
				<div id="menu1" class="tab" style="display:none">
					<ul class="sub05_box7_1" style="display:block;">
						<li>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img31.jpg" alt="" />
							<div class="sub05_box6">
							<p>Enter and manage maintenance record and expense in detail.</p>
							<p>Customer can check maintenance records on smartphone application</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div id="menu2" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img32.jpg" alt="" />
						<div class="sub05_box6">
						<p>Vehicle’s maintenance records Lookup is available by periodic terms, customer and payment method. (Maintenance Items,<br>Parts Grade/Price, wage, total charge, discount, etc.)</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu3" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img33.jpg" alt="" />
						<div class="sub05_box6">
						<p>Whole customer lists lookup is available by a specified attribute (ex: ViewCAR member, General member) and user can get<br> an idea of their repair shop client type.</p>
						<p>Customer lookup is available by name, plate number, and phone number. Further information on maintenance history is<br>available for ViewCAR members.</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu4" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img34.jpg" alt="" />
						<div class="sub05_box6">
						<p>Vehicle consumables lookup is available for ViewCAR members</p>
						<p>Consumables replacement period is available in bar graph and numeric values and information is available on both VClinic<br>and Application</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu5" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img35.jpg" alt="" />
						<div class="sub05_box6">
						<p>ViewCAR members can check repair shop reservation status and make a maintenance reservation to the designated repair<br>shop through an application. The reservation information is only available to the designated repair shop.</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu6" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img36.jpg" alt="" />
						<div class="sub05_box6">
						<p>Check the list of customers by group, service type, level, plate number, and name and send an one way message to customers.  
					</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu7" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img37.jpg" alt="" />
						<div class="sub05_box6">
						<p>Can make a comparison with other repair shop’s service and quotation. (ViewCAR Clinic enrolled shop)</p>
						<p>Share technological information, knowledge, and opinions among ViewCAR Clinic registered repair shops.</p>
						<p>By sharing the black consumer list, other repair shops can prevent unnecessary troubles.</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
	 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

		