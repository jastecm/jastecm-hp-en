<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_05");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			V Share
		</div>
		<div class="sub_title7_2">
			Car-Sharing Solution, V Share
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img38.jpg" alt="" /></div>
		<div class="sub_tit1">Korean Car-Sharing Solution</div>
		<div class="sub05_box6">
			<p>Complex Car-Sharing Solution Components and implementing process (Wage occurrence and Vehicle’s value drops)</p>
		</div>
		<ul class="sub05_box8">
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img39.jpg" alt="" />
				<p>CSA Main</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img40.jpg" alt="" />
				<p>GPS Antenna</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img41.jpg" alt="" />
				<p>RFID Reader</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img42.jpg" alt="" />
				<p>Main Cable</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img43.jpg" alt="" />
				<p>OBD Cable</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img44.jpg" alt="" />
				<p>Modem Antenna </p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img45.jpg" alt="" />
				<p>Wire to hold key</p>
			</li>
		</ul>
		<div class="sub05_box6">
			<p>Frequent compatibility error between Application and Device</p>
			<p>RFID Necessary</p>
			<p>Limited Compatible Vehicle Models</p>
			<p>Inaccurate RPM and mileage due to frequent omission of sensor and driving data</p>
		</div>
		<br><br><br><br><br><br><br>
		<div class="sub_tit1">Car-Sharing Solution V Share Features</div>
		<p> </p>
		<div class="sub05_box6">
			<p>Establish Car-Sharing Solution with VShare  OBD-II device (On-Board/On-line Types)</p>
		</div>
		<ul class="sub05_box8">
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img46.jpg" alt="" />
				<p>V Share Device</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub05_img47.jpg" alt="" />
				<p>OBD-II Installation </p>
			</li>
		</ul>
		<div class="sub05_box6">
			<p>Flexible service available with MQTT Protocol and specialized program within the device</p>
			<p>Provides Remote Starting Control, Smart Key, RFID Functions</p>
			<p>eCall service available with G-Sensor & Airbag deployment sensor Data</p>
			<p>Various Compatible Vehicle Models</p>
			<p>Precise Diagnosis result and management of 15 major consumables</p>
		</div>
	</div>