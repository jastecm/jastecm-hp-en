<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_03");
});

$(document).ready(function(){
	$(".toggleMenu").on("click",function(){
		$(".toggleMenu").removeClass("active");
		$(this).addClass('active');
		$(".tabbable").find(".tab").hide();
		tab = $(this).data("menu");
		
		$(tab).show();
		
	});
	
	$(".toggleMenu").eq(0).trigger("click");
	
});
</script>
	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			FMS
		</div>
		<div class="sub_title7_2">
			FMS (Fleet Management System) Solution, VDAS
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img23.jpg" alt="" /></div>
		<div class="sub_tit1">Service Outline</div>
		<div class="sub05_box6 mab80">
			<p>
				Developed to meet the service demands of  Fleet Management, preventing speeding and driving behavior analysis.
				<span>
					- Participated in 2015~2016 K-Global Smart mobile support project.<br>
					- Co-developed Dangerous Driving Analysis Solution with KoROAD based on TAAS.
				</span>
			</p>
			<p>Specialized in trip record service for Tax deduction.</p>
			<p>Provides solution and ASP service to client companies.</p>
		</div>
		<div class="sub_tit1">Service Effectiveness</div>
		<ul class="sub05_box6 mab70">
			<li>
				<p>ECO Driving→ <b>Reduce fuel cost</b></p>
				<p>Trip & Stop Monitoring→ <b>Reduce Idle Time</b></p>
				<p>Reduce Non-Business Use→ <b>Reduce Trip distance</b></p>
				<p>Efficient Vehicle Allocation→ <b>Increase Vehicle utilization</b></p>
				<p>Simplified Allocation Service→ <b>Increase work efficiency  </b></p>
			</li> 
			<li>
				<p>Driving Behavior Monitoring & Fine Management→ <b>Reduce Speeding</b></p>
				<p>Driving Behavior Analysis→ <b>Good Driving Behavior Increase</b></p>
				<p>Trip Record, Maintenance Management→ <b>Increase Vehicle Management Efficiency </b></p>
				<p>Efficient vehicle garage management→ <b>Increase efficiency </b></p>
			</li>
		</ul>
		<div class="sub_tit1">Service Details</div>

	<div class="tabbable"> 
		<ul class="tabs">
			<div class="sub_tab">
				<a data-menu="#menu1" class="toggleMenu" style="width: 130px">Platform</a><a data-menu="#menu2" class="toggleMenu" style="width: 130px">Trip Record</a><a data-menu="#menu3" class="toggleMenu" style="width: 130px">Expense</a><a data-menu="#menu4" class="toggleMenu" style="width: 130px">Trip Monitoring</a><a data-menu="#menu5" class="toggleMenu" style="width: 130px">Vehicle Allocation</a><a data-menu="#menu6" class="toggleMenu" style="width: 130px">Operation Consult</a>
			</div>
		</ul> 
		<div class="sub05_box7">
			<div class="tabcontent">
				<div id="menu1" class="tab" style="display:none">
					<ul class="sub05_box7_1" style="display:block;">
						<li>
							<img src="${pageContext.request.contextPath}/common/images/sub05_img24.jpg" alt="" />
							<div class="sub05_box6">
							<p>Establish DB system for vehicle Airbag SRS(Driver, Passenger, Side Airbag by Manufacturer, Type, Year), ABS, Immobilizer, BCM(Body Control Module)</p> 
							<p>Establish DB system by General/manufacturers/vehicle types for DTC and airbag deployment system.</p>
							<p>Vehicle Consumables DB from vehicle maintenance service</p>
							<p>Establish 2.3 million Accident Statistic DB(National Police Department) and 470 thousand GIS MAP DB(Ministry of Land, Infrastructure and Transport)</p>
							<p>Establish Trip, accident data Cloud Service</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div id="menu2" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img25.jpg" alt="" />
						<div class="sub05_box6">
						<p>Accurate Trip distance and duration record through Automotive-ICT device</p>
						<p><b>Trip report auto record</b> through VDAS</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu3" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img26.jpg" alt="" />
						<div class="sub05_box6">
						<p>Trip expense management by users and vehicles</p>
						<p>Efficient expense management by expense comparative analysis</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu4" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img27.jpg" alt="" />
						<div class="sub05_box6">
						<p><b>ECO*Safe Driving Coaching</b> by real time monitoring</p>
						<p>Cost reduction by ECO*Safe Driving</p>
						<p>Keep Vehicle in good condition with vehicle consumables management service.</p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu5" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img28.jpg" alt="" />
						<div class="sub05_box6">
						<p><b>VDAS vehicle allocation service is based on Car-Sharing Service and available on both web and app.</b></p>
						</div>
					</li>
				</ul>
			</div>
			<div id="menu6" class="tab" style="display:none">
				<ul class="sub05_box7_1" style="display:block;">
					<li>
						<img src="${pageContext.request.contextPath}/common/images/sub05_img29.jpg" alt="" />
						<div class="sub05_box6">
						
						</div>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
</div>
		
		
		
		
		