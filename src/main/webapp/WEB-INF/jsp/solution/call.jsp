<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_07");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			V Call
		</div>
		<div class="sub_title7_2">
			e-Call(Emergency Call) Solution. V Call
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img51.jpg" alt="" /></div>
		<div class="sub_tit1">Service Outline</div>
		<div class="sub05_box6">
			<p>Aftermarket IoT devices that detects accidents and makes an emergency call.</p>
			<p>ETSI, CEN IVS(BM e-Call) related ISO in progress. GRSG AECS informal Group AECD(Accident Emergency Call Device) Task Force was formed under UN to establish the standard.</p>
			<p>e-Call legislation is planned in 2018~2019 by the Ministry of Land, Infrastructure and Transport.</p>
			<p><strong class="s1">JastecM’s Patent based ISO/WD 20530 is in progress.</strong></p>
		</div>
		<br><br><br><br><br>
		<div class="sub_tit1">Focusing Scopes</div>
		<img style="margin-left:120px;" src="${pageContext.request.contextPath}/common/images/sub05_img52.jpg" alt="" />
		<br><br><br><br><br>
		<div class="sub_tit1">Effectiveness</div>
		<div class="sub05_box6">
			<p>1ST Place in the OECD in terms of Traffic accident casualty (2.6 Casualties per 10 thousand vehicles)</p>
			<p>Accord with the Korean MOLIT’s strategy of <strong class="s1">Traffic Accident Casualties Rate 30% drop by 2017</strong></p>
			<p>Reduce in accident expense annually (13 Trillion KRW is spent on accident injured and casualty out of 23 Trillion KRW social expense)</p>
			<p>Casualties drop by respond within “Golden Time”</p>
			<p>50% time reduction in accident response time with e-Call service.</strong></p>
		</div>
		<br><br><br><br><br><br>
		<div class="sub_tit1">Ultimate Goal</div>
		<img style="margin-left:120px;" src="${pageContext.request.contextPath}/common/images/sub05_img53.jpg" alt="" />
	</div>