<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_08");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			V Comm
		</div>
		<div class="sub_title7_2">
			V2X based Solution, V COMM
		</div>
		<div><img src="${pageContext.request.contextPath}/common/images/sub05_img54.jpg" alt="" /></div>
		<div class="sub_tit1">Service Outline</div>
		<div class="sub05_box6">
			<p>WAVE (Wireless Access in Vehicular Environment) is IEEE technological standard and essential technology of an accident prevention service</p>
			<p>5.8GHz bandwidth RF, OFDM Modem, MAC routing technology are necessary in short ranged high mobility(Max 200km/h) network among vehicles. It is based on IEEE 802.11a/g wireless LAN broadcasting technology.</p>
		</div>
		<br><br><br><br><br>
		<div class="sub_tit1">Focusing Scopes</div>
		<img style="margin-left: 120px" src="${pageContext.request.contextPath}/common/images/sub05_img55.jpg" alt="" />
		<br><br><br><br><br>
		<div class="sub_tit1">Service Features</div>
		<img style="margin-left: 120px" src="${pageContext.request.contextPath}/common/images/sub05_img56.jpg" alt="" />
		<br><br><br><br><br><br>
		<div class="sub_tit1">VCOMM Solution Field & Strength</div>
		<div class="sub05_box6">
			<p><strong class="s1">Collect/Analysis/Process Speed, RPM, transmission, direction, brake, trouble code, accident, danger notice(Airbag deployment, emergency light), passengers data through OBD-II in 10~100 msec.</strong></p>
			<p>All models after 2006 are compatibles (2001 model year For U.S. & E.U.)</p> 
			<p>SAE international standard support (ISO 9141-2, ISO 14230-4(KWP-2000), ISO 15765-4(CAN), SAE J1850(PWM/VPWM)</p>
			<p>Close relation with WAVE Chipset manufactures</p>
			<p>Established a global network of ITS Service Develop & evaluation Provider</p>
		</div>
	</div>