<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM05");
	initMenuSel("LM05_02");
});
</script>
	<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			Application
		</div>
		<div class="sub_title7_2">
			Vehicle Management Application Solution, VSAPP
		</div>
		<div class="mab90"><img src="${pageContext.request.contextPath}/common/images/sub05_img9.jpg" alt="" /></div>
		<div class="sub_tit1">Features</div>
		<div class="sub05_box4">
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img10.jpg" alt="" /></dt>
				<dd>
					<h1>Simple Timeline</h1>
					<p>
						Every service and event is<br>recorded and can be sorted<br>by date or options.
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img11.jpg" alt="" /></dt>
				<dd>
					<h1>Safe*Eco Driving Aid</h1>
					<p>
						Service publicness has<br>acquired by applying the<br>scoring Index developed by<br>KOROAD on VDAS.
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img12.jpg" alt="" /></dt>
				<dd>
					<h1>Door Lock/Unlock</h1>
					<p>
						<span>World’s First</span> OBD device that enables Door Lock/Unlock Function without wire
(activates within 5~10m range near the vehicle)
					</p>
				</dd>
			</dl>
		</div>
		<div class="sub05_box4">
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img13.jpg" alt="" /></dt>
				<dd>
					<h1>Parked Car Locator/ Alarm</h1>
					<p>
						Record and mark the last location<br>of vehicle as a parked lot. Set<br>parking hours then notify driver<br>before parking hours run out.
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img14.jpg" alt="" /></dt>
				<dd>
					<h1>HUD</h1>
					<p>
						HUD is available in Horizontal &<br>Vertical Interface and reads various<br>vehicle sensor and driving data
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img15.jpg" alt="" /></dt>
				<dd>
					<h1>Diagnosis/MyCar Clinic</h1>
					<p>
						Check vehicle components<br>functionality by activating diagnosis<br>on 3,000~7,000 items and Book<br>Maintenance Service through<br>My Car Clinic.
					</p>
				</dd>
			</dl>
		</div>
		<div class="sub05_box4 mab35">
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img16.jpg" alt="" /></dt>
				<dd>
					<h1>Consumables Management</h1>
					<p>
						Notifies Consumables<br>Replacements Period based on<br>Mileage and provides benefits and<br>services through Commerce Service
					</p>
				</dd>
			</dl>
			<dl>
				<dt><img src="${pageContext.request.contextPath}/common/images/sub05_img17.jpg" alt="" /></dt>
				<dd>
					<h1>e-Call</h1>
					<p>
						Emergency SMS is sent<br>automatically in case of Airbag<br>Deployed Accident. JastecM’s<br>ISO/NP20530 International<br>Standardization of e-Call in progress
					</p>
				</dd>
			</dl>
		</div>
		<div class="sub_tit1">App Auto-Launch</div>
		<ul class="sub05_box5">
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img18.jpg" alt="" /></div>
				<p>Connect VID to Vehicle</p>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img22.jpg" alt="" /></div>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img19.jpg" alt="" /></div>
				<p>Detect Engine on/off<br>(Auto detecting within 1~2 seconds)</p>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img22.jpg" alt="" /></div>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img20.jpg" alt="" /></div>
				<p>App Auto Launch and Close</p>
			</li> 
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img22.jpg" alt="" /></div>
			</li>
			<li>
				<div><img src="${pageContext.request.contextPath}/common/images/sub05_img21.jpg" alt="" /></div>
				<p>Trip Record &<br>Parking Location Record</p>
			</li>
		</ul>
		<div class="sub05_box6 mab80">
			<p>Driver only needs to concentrate on safe driving with APP auto-launch.</p>
			<p>Low battery consumption by memory optimization</p>
		</div>
		<div class="sub_tit1">VSAPP Strategy</div>
		<div class="sub05_box6">
			<p>VSAPP supports all von-Series and plans to extend service to other industry’s devices</p>
			<p>VSAPP features have been developed throughout 2015~2016 K-Global Smart mobile support project.</p>
			<p>Also available as an ASP(Application Service Provider) product.</p>
		</div>
	</div>
	