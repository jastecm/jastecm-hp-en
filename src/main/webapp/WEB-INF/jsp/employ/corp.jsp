<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM07");
	initMenuSel("LM07_01");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			Our Company
		</div>
		<div class="sub_title6 mab13">
			<span>Development Policy</span><strong></strong>
		</div>
		<div class="sub_txt2">JastecM runs program that every employee can strengthen their ability</div>
		<ul class="sub05_box1">
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub07_img1.jpg" alt="" />
				<h1>To the Center of the World!</h1>
				<p>
				
				JastecM encourages employees to cooperate with the<br>other group members to solve the problem and to study<br> for their education. JastecM supports employees to<br> participate in every domestic and overseas conference.
				</p>
			</li>
			<li>
				<img src="${pageContext.request.contextPath}/common/images/sub07_img2.jpg" alt="" />
				<h1>Paradigm Shift </h1>
				<p>
					JastecM values even the tiniest ideas. Wide perspective of<br> ideas creates new paradigm. JastecM believes collected<br> ideas will bring the change of civilization. As a way to<br> motivate JastecM employees, JastecM runs an expertise<br> enhancement program as well as strengthen ability<br> program. In addition, JastecM offers flexible working<br> hours policy so the employee can spend their time efficiently.
				</p>
			</li>
		</ul>
		<div class="sub_title6 mab13">
			<span>Welfare</span><strong></strong>
		</div>
		<div class="sub_txt2">JastecM provides welfare program to employees for their benefit</div>
		<ul class="sub07_box1">
			<li>
				<span>Welfare<br>&<br>Vacation</span>
				<p>
					Five-day workweek / Flexible working hours<br>
14 days vacation annually, 1 day addition after 2 years of service<br>
Offers sabbatical leave every 5 years of service as well as vacation bonus
				</p>
			</li>
			<li>
				<span>Amenities</span>
				<p>
					Cafeteria, Fitness Center
Provide a place for brainstorming
Vacation Condominium Provision

				</p>
			</li>
			<li>
				<span>Insurance</span>
				<p>Every employees are insured providing cost coverage for injury, accident, disease.</p>
			</li>
			<li>
				<span>Stock Option &<br>
Bonus</span>
				<p>
					Employees with 3 years of service is offered with a stock option.<br>
Bonus is given based on fair assessment.
				</p>
			</li>
			<li>
				<span>Promotion</span>
				<p>
					Promotion in every year<br>
Special promotion is guaranteed for outstanding employees.
				</p>
			</li>
			<li>
				<span>Expenditure<br>Support</span>
				<p>
					Support any expenditures including meal, fuel, and congregate meeting
				</p>
			</li>
			<li>
				<span>Condolences &<br> Congratulations<br> Expenditure</span>
				<p>
					Offers Condolences and congratulations gratuity and holiday presents
				</p>
			</li>
		</ul>
	</div>