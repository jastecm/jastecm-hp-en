<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM07");
	initMenuSel("LM07_02");
});

</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			Message from Board of Directors
		</div>
		<div class="sub_title7_2">
			Change of thought in scientific revolution is unavoidable. JastecM believes<br> further discussion and cooperation on searching for the new vision is necessary<br> to bring <span>the genuine 4th Industrial Revolution.</span> 
		</div>
		<div class="sub07_box2">
			<img src="${pageContext.request.contextPath}/common/images/sub07_img3.jpg" alt="" />
			<h1>주행주행자동차의 도로운행에 대한 법적 고찰</h1>
			<p>
				과학혁명의 새로운 사고전환이 이뤄지고 요구되는 시기입니다.<br>
				새로운 가치 창출을 위한 비전이 공유되고 토론 및 협업이 이뤄질 때,<br>
				진정한 4차 산업혁명은 올 것입니다.<br>
				과학혁명의 새로운 사고전환이 이뤄지고 요구되는 시기입니다.<br>
				새로운 가치 창출을 위한 비전이 공유되고 토론 및 협업이 이뤄질 때,<br>
				진정한 4차 산업혁명은 올 것입니다.<br>
				과학혁명의 새로운 사고전환이 이뤄지고 요구되는 시기입니다.<br>
				새로운 가치 창출을 위한 비전이 공유되고 토론 및 협업이 이뤄질 때,<br>
				진정한 4차 산업혁명은 올 것입니다. 
			</p>
			<a href="#"><span>+</span>더보기</a>
		</div>
		<table id="table" class="table1">
			<colgroup>
				<col width="80">
				<col width="*">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th>No</th>
					<th>Title</th>
					<th>Date</th>
					<th>Writer</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${fn:length(rtvList) > 0 }">
					<c:forEach var="vo" items="${rtvList}" varStatus="i">
						<tr class="f">
							<th>${vo.rNum }</th>
							<td style='text-align:center'><a href="${pageContext.request.contextPath}/employ/ceomsgDetail.do" class="callPage">${vo.title }</a></td>
							<th><fmt:parseDate var="d" value="${vo.regDate }" pattern="yyyy-MM-dd HH:mm:ss.S"/>
							<fmt:formatDate value="${d}" pattern="yyyy-MM-dd"/></th>
							<th>${vo.id}</th>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		<%@ include file="/WEB-INF/jsp/common/PagingNavi.jsp"%>
		<div class="bo_view_btn">
			<a href="#"><img src="${pageContext.request.contextPath}/common/images/prev3.jpg" alt="" /></a><a href="#"><img src="${pageContext.request.contextPath}/common/images/next3.jpg" alt="" /></a>
			<span><a href="${pageContext.request.contextPath}/employ/ceomsg.do" class="callPage">List</a></span>
		</div>
	</div>