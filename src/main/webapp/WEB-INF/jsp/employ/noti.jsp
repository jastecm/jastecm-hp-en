<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM07");
	initMenuSel("LM07_04");
});

</script>
 
<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			Recruit
		</div>
		<div class="sub_txt2">JastecM is looking for a creative talent who can lead the new revolution</div>
		<div class="sub06_box1">
			<p>For a recruit inquiry please contact job@Jastecm.com</p>
			<p>There is no form or application made. Please create your own portfolio to express yourself.</p>
			<p>Application will only be used for entering and application will not be returned.</p>
			<p>Preferences will be given to Disabled/Veterans.</p>
		</div>
		<div class="sub_title6 mab13">
			<span>Recruit Notice</span><strong></strong>
		</div>
		<br><br>
		<table id="table" class="table1 table1_faq">
			<colgroup>
				<col width="139" />
				<col width="*" />
			</colgroup>
			<thead>
				<tr>
					<th>No</th>
					<th>Title</th>
					<th>Term of recruit</th>
					<th>date</th>
				</tr>
			</thead>
			<tbody>
				
				<c:if test="${fn:length(rtvList) > 0 }">
					<c:forEach var="vo" items="${rtvList}" varStatus="i">
						<tr class="f">
							<th>${vo.contentTypeNm }</th>
							<td style='text-align:center'>${vo.title }</td>
							<th>${vo.boardType }</th>
							<th><fmt:parseDate var="d" value="${vo.regDate }" pattern="yyyy-MM-dd HH:mm:ss.S"/>
							<fmt:formatDate value="${d}" pattern="yyyy-MM-dd"/></th>
						</tr>
						<tr class="a">
							<th></th>
							<td style='text-align:center'>
								${vo.contents }
							</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		<%-- <div class="paging">
			<a href="#">
				<img src="${pageContext.request.contextPath}/common/images/first.jpg" alt="">
			</a><a href="#">
				<img src="${pageContext.request.contextPath}/common/images/prev2.jpg" alt="">
			</a><span>
				<a href="#" class="on">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#">6</a><a href="#">7</a><a href="#">8</a><a href="#">9</a><a href="#">10</a>
			</span><a href="#">
				<img src="${pageContext.request.contextPath}/common/images/next2.jpg" alt="">
			</a><a href="#">
				<img src="${pageContext.request.contextPath}/common/images/last.jpg" alt="">
			</a>
		</div> --%>
		<br><br><br><br><br><br>
		<div class="sub_title6">
			<span>Recruit Process</span><strong></strong>
		</div>
		<div class="sub07_box7">
			<span>
				Application
			</span><img src="${pageContext.request.contextPath}/common/images/arrow2.jpg" alt="" /><span>
				Screening
			</span><img src="${pageContext.request.contextPath}/common/images/arrow2.jpg" alt="" /><span>
				Interview
			</span><img src="${pageContext.request.contextPath}/common/images/arrow2.jpg" alt="" /><span class="s1">
				Board<br>Interview
			</span><img src="${pageContext.request.contextPath}/common/images/arrow2.jpg" alt="" /><span class="last">
				Accepted
			</span>
			<p>※ Recruit process depends on division.</p>
		</div>
	</div>