<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM07");
	initMenuSel("LM07_03");
});
</script>

<div class="sub_layout">
		<div class="sub07_box3">
			<div class="sub07_box3_1">
				<div class="sub_title1">
					<span></span>
					Recent Events
				</div>
				<div class="sub07_box4">
					<h1>주행주행자동차의 도로운행에 대한 법적 고찰</h1>
					<span>2016.06.28</span>
					<p>과학혁명의 새로운 사고전환이 이뤄지고 요구되는 시기입니다. 새로운 가치 창출을 위한 비전이 공유되고 토론 및 협업이 이뤄질 때, 진정한 4차 산업혁명은 올 것입니다. 
과학혁명의 새로운 사고전환이 이뤄지고 요구되는 시기입니다.
새로운 가치 창출을 위한 비전이 공유되고 토론 및 협업이 이뤄질 때, 진정한 4차 산업혁명은 올 것입니다.  과학혁명의 새로운 사고전환이 이뤄지고 요구되는 시기입니다.
새로운 가치 창출을 위한 비전이 공유되고 토론 및 협업이 이뤄질 때, 진정한 4차 산업혁명은 올 것입니다.</p>
					<div class="img">
						<img src="${pageContext.request.contextPath}/common/images/sub07_img5.jpg" alt="" />
					</div>
					<h1>주행주행자동차의 도로운행에 대한 법적 고찰</h1>
					<span>2016.06.28</span>
					<p>과학혁명의 새로운 사고전환이 이뤄지고 요구되는 시기입니다. 새로운 가치 창출을 위한 비전이 공유되고 토론 및 협업이 이뤄질 때, 진정한 4차 산업혁명은 올 것입니다. 
과학혁명의 새로운 사고전환이 이뤄지고 요구되는 시기입니다.
새로운 가치 창출을 위한 비전이 공유되고 토론 및 협업이 이뤄질 때, 진정한 4차 산업혁명은 올 것입니다.  과학혁명의 새로운 사고전환이 이뤄지고 요구되는 시기입니다.
새로운 가치 창출을 위한 비전이 공유되고 토론 및 협업이 이뤄질 때, 진정한 4차 산업혁명은 올 것입니다.</p>
					<div class="img">
						<img src="${pageContext.request.contextPath}/common/images/sub07_img5.jpg" alt="" />
					</div>
					<div class="paging paging2">
						<strong class="s1"><a href="#">
							<img src="${pageContext.request.contextPath}/common/images/first.jpg" alt="">
						</a><a href="#">
							<img src="${pageContext.request.contextPath}/common/images/prev2.jpg" alt="">
						</a></strong><span>
							<a href="#" class="on">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a>
						</span><strong class="s2"><a href="#">
							<img src="${pageContext.request.contextPath}/common/images/next2.jpg" alt="">
						</a><a href="#">
							<img src="${pageContext.request.contextPath}/common/images/last.jpg" alt="">
						</a></strong>
					</div>
				</div>
			</div>
			<div class="sub07_box3_2">
				<div class="sub_tit1">Visit Our Blog</div>
				<div class="sub07_box5">
					<p>
						뷰카 블로그에서 자스텍M의 다양한 소식을 만나보실 수 있습니다. 아래 바로가기를 누르시면 네이버 공식 블로그로 이동합니다.
					</p>
					<a href="#" class="btn1">Shortcut</a>
				</div>
				<div class="sub_tit1">Popular Post</div>
				<div class="sub07_box5">
					<p class="p1">
						<a href="#">새로운 뷰카기능을 쉽게 알려드려요!</a>
						<span>2016.06.28</span>
					</p>
					<div class="img"><a href="#"><img src="${pageContext.request.contextPath}/common/images/sub07_img6.jpg" alt="" /></a></div>
				</div>
				<div class="sub_tit1">Recent Post</div>
				<ul class="sub07_box6">
					<li>
						<div class="img"><a href="#"><img src="${pageContext.request.contextPath}/common/images/sub07_img7.jpg" alt="" /></a></div>
						<p><a href="#">오늘의 판교 출근길 풍경 전해드려요.</a></p>
						<span>2016.06.28</span>
					</li>
					<li>
						<div class="img"><a href="#"><img src="${pageContext.request.contextPath}/common/images/sub07_img8.jpg" alt="" /></a></div>
						<p><a href="#">오늘의 판교 출근길 풍경 전해드려요.</a></p>
						<span>2016.06.28</span>
					</li>
					<li>
						<div class="img"><a href="#"><img src="${pageContext.request.contextPath}/common/images/sub07_img7.jpg" alt="" /></a></div>
						<p><a href="#">오늘의 판교 출근길 풍경 전해드려요.</a></p>
						<span>2016.06.28</span>
					</li>
					<li>
						<div class="img"><a href="#"><img src="${pageContext.request.contextPath}/common/images/sub07_img8.jpg" alt="" /></a></div>
						<p><a href="#">오늘의 판교 출근길 풍경 전해드려요.</a></p>
						<span>2016.06.28</span>
					</li>
				</ul>
				<div class="search_box2">
					<input type="text" placeholder="Search post" /><button>Search</button>
				</div>
			</div>
		</div>
	</div>