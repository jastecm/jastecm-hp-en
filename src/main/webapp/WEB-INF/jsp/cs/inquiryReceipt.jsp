<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	$("#btn_send").on("click",function(){
		var frmParam = $("#frm").serializeArray();
		var param = {};
		for(var i= 0 ; i < frmParam.length ; i++){
			if(param[frmParam[i].name]) param[frmParam[i].name] += ","+frmParam[i].value
			else param[frmParam[i].name] = frmParam[i].value
		}
		param.callPage = "/cs/inquiryReceiptFinish";
		var url = "/index.do";
		$JBIZ.instance_post(url,param);
	});
});
</script>

<div class="sub_layout">
		<div class="sub_title5 mab68">
			Receipt Confirmation
		</div>
		<div class="sub_tit1">Content</div>
		<form id="frm">
			<table class="table2 table2_2 mab76">
				<colgroup>
					<col width="73" />
					<col width="145" />
					<col width="52" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>Name</th>
						<td>${reqVo.name}<input type="hidden" name="name" value="${reqVo.name}"></td>
						<th>E-mail</th>
						<td>${reqVo.email}<input type="hidden" name="email" value="${reqVo.email}"></td>
					</tr>
					<tr> 
						<th>Title</th>
						<td colspan="3">${reqVo.title}<input type="hidden" name="title" value="${reqVo.title}"></td>
					</tr>
					<tr>
						<th>Content</th>
						<td colspan="3">
							${reqVo.contents}
							<input type="hidden" name="contents" value="${reqVo.contents}">
						</td>
					</tr>
					<tr>
						<th>File</th>
						<td colspan="3">
							<c:set var="fileList" value="${fn:split(reqVo.file,',')}" />
							<c:set var="fileNmList" value="${fn:split(reqVo.fileNm,',')}" />
							<c:forEach var="vo" items="${fileList}" varStatus="i">
								<input type="hidden" name="file" value="${vo}">
							</c:forEach>
							<c:forEach var="vo" items="${fileNmList}" varStatus="i">
								<c:if test="${i.index eq 0}">
									<a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
								</c:if>
								<c:if test="${i.index ne 0}">
									</br><a href="${pageContext.request.contextPath}/com/getFile.do?uuid=${fileList[i.index]}">${vo }</a>
								</c:if>
							</c:forEach>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		<div class="sub_txt1 mab50">
			<p>등록된 문의사항은 5일(영업일 기준)이내 처리를 기본으로 하고 개인정보는 본인확인 및 단변 전송용도로만 이용됩니다.</p>
			<p>문의내용에 대한 검토 결과를 담당자가 회신드리겠습니다.</p>
		</div>
		<div class="btn_area1">
			<a href="${pageContext.request.contextPath}/cs/inquiry.do" class="callPage">Cancle</a>
			<a id="btn_send">Submit</a>
		</div>
	</div>
