<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script src='${pageContext.request.contextPath}/common/js/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM08");
	initMenuSel("LM08_02"); 
	
	$('#frm').ajaxForm({
		beforeSubmit: function (data,form,option) {
			return true;
		},
		success: function(res,status){
	    	
			var rtvCd = res.rtvCd;
			if(rtvCd == "0"){
				alert("error");
			}else{
				var url = "/index.do";
				res.rtvMap.callPage = "/cs/inquiryReceipt";
				$JBIZ.instance_post(url,res.rtvMap);
			}
		},
		error: function(){
	    	alert("error");
		}                               
	});
	
	$("#frm").submit(function(){return false});
	
	$("#btn_send").on("click",function(){
		if(!($("#input-1").is(":checked"))){ 
			alert("개인정보 수집 이용안내에 동의해 주세요.");
			return false;
		}
		$('#frm').submit();
	});
});
</script>

<div class="sub_layout"> 
		<div class="sub_title1">
			<span></span>
			1:1 Inquiry
		</div>
		<div class="sub_title4">
			Please free to contact us if you are having a trouble finding your answer.<br> We are happy to assist you.

		</div>
		<div class="sub06_box1">
			<p>Please search your answer from FAQ page first before making contact</p>
			<p>Your personal information will only be used for the verification and sending response </p>
		</div> 
		<form id="frm" action="${pageContext.request.contextPath}/com/fileUpload.do" method="post" enctype="multipart/form-data">		
		<div class="sub_tit1">문의내용</div>
		<table class="table2 mab76">
			<colgroup>
				<col width="73" />
				<col width="345" />
				<col width="52" />
				<col width="*" />
			</colgroup>
			<tbody>
				<tr>
					<th>Name</th>
					<td><input type="text" class="w316" name="name" placeholder="문의자분의 이름을 입력해주세요." /></td>
					<th>E-mail</th>
					<td><input type="text" class="w331" name="email" placeholder="답변받으실 이메일을 입력해주세요." /></td>
				</tr>
				<tr>
					<th>Title</th>
					<td colspan="3"><input type="text" class="w728" name="title" placeholder="제목을 입력해주세요." /></td>
				</tr>
				<tr>
					<th>Content</th>
					<td colspan="3"> 
						<textarea name="contents"></textarea>
					</td>
				</tr>
				<tr>
					<th>File</th>
					<td colspan="3">
						<div class="btn_file"><input type="file" name="file" class="multi" maxlength="3"/></div>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="sub_tit1">Consent to Collect, Use and Disclose Personal Information </div>
		<div class="sub06_box2">
			The personal information provided within this application is collected by JastecM.<br>
			1. Collecting Personal Information Items : Company, Name, e-mail, Phone<br>
			2. Purpose :  To confirm the identification of proposer and to make a contact<br>
			3. Retention Period :  PI will be terminated after a month<br>
			4. Right to refuse consent : Proposer may refuse to consent and restricted to make a proposal<br>
JastecM complies with Personal Information Protection Policy.
		</div>
		<div class="agree_box demo-list mab63">
			<input tabindex="1" type="checkbox" id="input-1"><label for="input-1">Yes I have read and I agree</label>
		</div>
		</form> 
        
     	<div class="btn_area1">
			<a href="${pageContext.request.contextPath}/cs/inquiry.do" class="callPage">Cancle</a>
			<a href="#none" id="btn_send">Submit</a>
		</div>
	</div>
	