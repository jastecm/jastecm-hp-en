<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page pageEncoding="UTF-8" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/PageCommon.js" ></script>
<c:if test="${pageNavi.pageTotal != 1}">
	<c:if test="${pageNavi.totalSize>0}">
				<div class="paging">				
		<c:if test="${pageNavi.currentPage-10>=1}">
					<a href="#none" class="controlPage" onclick="goMoveList(<c:out value="${1}"/>);return false;">						
						<img src="${pageContext.request.contextPath}/common/images/first.jpg" alt=""> <!-- first -->
					</a>
		</c:if>
		<c:if test="${pageNavi.currentPage-10< 1}">
			<c:if test="${pageNavi.currentPage == 1}">
					<a href="#none" class="controlPage">
						<img src="${pageContext.request.contextPath}/common/images/first.jpg" alt=""> <!-- first -->
					</a>
			</c:if>
			<c:if test="${pageNavi.currentPage != 1}">
					<a href="#none" class="controlPage" onclick="goMoveList(<c:out value="${1}"/>);return false;">
						<img src="${pageContext.request.contextPath}/common/images/first.jpg" alt=""> <!-- first -->
					</a>
			</c:if>					
		</c:if>
		<c:if test="${pageNavi.currentPage-10>=1}">
					<a href="#none" onclick="goMoveList(<c:out value="${pageNavi.pageGroupStart -1}"/>);return false;">
						<img src="${pageContext.request.contextPath}/common/images/prev2.jpg" alt="">
					</a>
		</c:if>
		<c:if test="${pageNavi.currentPage-10< 1}">
			<c:if test="${pageNavi.currentPage == 1}">
					<a href="#none">
						<img src="${pageContext.request.contextPath}/common/images/prev2.jpg" alt="">
					</a>
			</c:if>
			<c:if test="${pageNavi.currentPage != 1}">
					<a href="#none" onclick="goMoveList(<c:out value="${pageNavi.pageGroupStart -1}"/>);return false;">
						<img src="${pageContext.request.contextPath}/common/images/prev2.jpg" alt="">
					</a>
			</c:if>
		</c:if>
		
		<span>
		
		<c:forEach var="idx" begin="${pageNavi.pageGroupStart}" end="${pageNavi.pageGroupEnd}">
			<c:if test="${idx == pageNavi.currentPage}">
				<a class="on" href="#none"> <c:out value="${idx}"/> </a>
			</c:if>
			<c:if test="${idx != pageNavi.currentPage}">
				<a class="" href="#none" onclick="goMoveList(<c:out value="${idx}"/>);return false;"> <c:out value="${idx}"/> </a>
			</c:if>
		</c:forEach>
		
		</span>
		
		<c:if test="${pageNavi.pageGroupStart+10<=pageNavi.pageTotal}">					
				<a href="#none" onclick="goMoveList(<c:out value="${pageNavi.pageGroupStart+10 }"/>);return false;">
					<img src="${pageContext.request.contextPath}/common/images/next2.jpg" alt=""> 
				</a>
		</c:if>
		<c:if test="${pageNavi.pageGroupStart+10>pageNavi.pageTotal}">
			<c:if test="${pageNavi.currentPage == pageNavi.pageTotal}">
				<a href="#none">
					<img src="${pageContext.request.contextPath}/common/images/next2.jpg" alt="">
				</a>					
			</c:if>
			<c:if test="${pageNavi.currentPage != pageNavi.pageTotal}">
				<a href="#none" onclick="goMoveList(<c:out value="${pageNavi.pageTotal}"/>);return false;">
					<img src="${pageContext.request.contextPath}/common/images/next2.jpg" alt="">
				</a>
			</c:if>
		</c:if>
		
		<c:if test="${pageNavi.pageGroupStart+10<=pageNavi.pageTotal}">
			<a href="#none" class="controlPage" onclick="goMoveList(<c:out value="${pageNavi.pageTotal}"/>);return false;">
				<img src="${pageContext.request.contextPath}/common/images/last.jpg" alt=""> <!-- last -->
			</a>
		</c:if>
		<c:if test="${pageNavi.pageGroupStart+10>pageNavi.pageTotal}">
			<c:if test="${pageNavi.currentPage == pageNavi.pageTotal}">
					<a href="#none" class="controlPage" >
						<img src="${pageContext.request.contextPath}/common/images/last.jpg" alt=""> <!-- last -->
					</a>
			</c:if>
			<c:if test="${pageNavi.currentPage != pageNavi.pageTotal}">
					<a href="#none" class="controlPage" onclick="goMoveList(<c:out value="${pageNavi.pageTotal}"/>);return false;">
						<img src="${pageContext.request.contextPath}/common/images/last.jpg" alt="">  <!-- last -->
					</a>
			</c:if>		
		</c:if>
	</div>
	
	<script type="text/javascript">
	/*
	if (pagingProperty && pagingProperty.searchForm && pagingProperty.searchForm.elements) {
		for(var i = 0; i < pagingProperty.searchForm.elements.length; i++) {
			var inputTag = document.createElement("INPUT");
			inputTag.type = "hidden";
			if (pagingProperty.searchForm.elements[i].type == "radio") {
				if (pagingProperty.searchForm.elements[i].checked == true) {
					inputTag.name = pagingProperty.searchForm.elements[i].name;
					inputTag.value = pagingProperty.searchForm.elements[i].value;
					document.pageNaviForm.appendChild(inputTag);	
				}
			} else if(pagingProperty.searchForm.elements[i].type == "checkbox") {
				if(pagingProperty.searchForm.elements[i].checked == true) {
					inputTag.name = pagingProperty.searchForm.elements[i].name;
					inputTag.value = pagingProperty.searchForm.elements[i].value;
					document.pageNaviForm.appendChild(inputTag);	
				}
			} else {
				inputTag.name = pagingProperty.searchForm.elements[i].name;
				inputTag.value = pagingProperty.searchForm.elements[i].value;
				document.pageNaviForm.appendChild(inputTag);
			}
		} 
	}
	*/
	</script>
	</c:if>
</c:if>
<form id="pageNaviForm" name="pageNaviForm" method="post" style="display:none" action="#none">
	<input type="hidden" name="currentPage" value="<c:out value="${pageNavi.currentPage}"/>" />
	<input type="hidden" name="rowsPerPage" value="<c:out value="${pageNavi.rowsPerPage}"/>" />
</form>
<script type="text/javascript" >
//alert("${pageNavi.pageTotal}");
</script>