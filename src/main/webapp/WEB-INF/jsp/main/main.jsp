<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM01");
});
</script>

<div class="main_banner_area">
	<div class="main_banner1"><img src="${pageContext.request.contextPath}/common/images/main_banner1.jpg" alt="" /></div>
	<ul class="main_banner2">
		<li>
			<a href="${pageContext.request.contextPath}/corporate/partnership.do" class="callPage">
				<h1 class="main_tit1">
					<span></span>
					Business Partnership
				</h1>
				<p>JastecM respects mutual cooperation<br>& shared value with anyone.</p>
			</a>
		</li>
		<li>
			<a href="${pageContext.request.contextPath}/cs/inquiry.do" class="callPage">
				<h1 class="main_tit1">
					<span></span>
					Service Inquiry
				</h1>
				<p>Having trouble with service?<br>We are glad to assist you.</p>
			</a>
		</li>
	</ul>
</div>
<div class="main_con_area">
	<div class="main_con_1">
		<h1 class="main_tit1">
			<span></span>
			Products
		</h1>
		<ul class="main-slide2">
			<li class="slide_1">
				<a href="${pageContext.request.contextPath}/product/type.do" class="callPage"><img src="${pageContext.request.contextPath}/common/images/main_img1.jpg" alt="" /></a>
				<div class="txt">
					<h1>von-S21 JTWM 1000 KT</h1>
					<p>GPG와 G-Sensor를 추가하여 사용자 편의성을 극대화한 제품입니다. </p>
				</div>
			</li>
			<li class="slide_1">
				<a href="${pageContext.request.contextPath}/product/type.do" class="callPage"><img src="${pageContext.request.contextPath}/common/images/main_img2.jpg" alt="" /></a>
				<div class="txt">
					<h1>von-STD JTBT 2200</h1>
					<p>Bluetooth를 추가하여 사용자 편의성을 극대화한 제품입니다. </p>
				</div>
			</li>
		</ul>
	</div>
	<div class="main_con_2">
		<h1 class="main_tit1">
			<span></span>
			News
		</h1>
		<div class="news_area mab52">
			<h1><a href="${pageContext.request.contextPath}/news/news.do" class="callPage">It has launched JASTECM Website.</a></h1>
			<p><a href="${pageContext.request.contextPath}/news/news.do" class="callPage">It has launched JASTECM Website.</a></p>
			<span>2016.06.01</span>
		</div>
		<div class="news_area">
			<h1><a href="${pageContext.request.contextPath}/news/news.do" class="callPage">It has launched JASTECM Website.</a></h1>
			<p><a href="${pageContext.request.contextPath}/news/news.do" class="callPage">It has launched JASTECM Website.</a></p>
			<span>2016.06.01</span>
		</div>
	</div>
	<div class="main_con_3">
		<img src="${pageContext.request.contextPath}/common/images/main_banner2.jpg" alt="" />
		<div class="news_area">
			<h1><a href="${pageContext.request.contextPath}/employ/today.do" class="callPage">JASTECM PROMOTION</a></h1>
			<p><a href="${pageContext.request.contextPath}/employ/today.do" class="callPage">JASTECM Promotion is coming soon. </a></p>
			<span>2016.06.01</span>
		</div>
	</div>
</div>
<div class="main_con_area2">
	<div class="main_con_4">
		<h1>RECRUIT</h1>
		<p>
			<a href="${pageContext.request.contextPath}/employ/noti.do" class="callPage">
				JastecM is looking for a talent <br>
				who wants to grow together.<br>
				JastecM provides programs for<br>
				career development as well as<br>
				labor welfare.
			</a>
		</p>
	</div>
	<div class="main_con_5">
		<h1>PARTNERS</h1>
		<ul class="main-slide2">
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img1.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img2.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img3.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img4.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img5.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img6.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img7.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img8.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img9.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img10.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img11.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img12.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img13.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img14.jpg" alt="" /></a>
				<a><img src="${pageContext.request.contextPath}/common/images/site_img15.jpg" alt="" /></a>
			</li>
			<li class="slide_1">
				<a><img src="${pageContext.request.contextPath}/common/images/site_img16.jpg" alt="" /></a>
			</li>
		</ul>
	</div>
</div>