<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM06");
	initMenuSel("LM06_04");
});
</script>

<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		Car Compatibility
	</div>
	<div class="video_area mab68">
		<img src="${pageContext.request.contextPath}/common/images/img_carCompatibility.png" style="width:1220px;height:500px;" />
	</div>
	<div class="sub04_box1" style="width:540px;height:480px;margin: auto;" >
		<iframe src="${pageContext.request.contextPath}/service/carCompatibilityFrame.do" style="width:100%;height:100%"; ></iframe>
	</div>
