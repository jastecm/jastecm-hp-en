<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="생활에서~경제로! 자동차Iot전문기업">

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
var _defaultPath = "${path}";
</script>

<script charset="utf-8" src="${pageContext.request.contextPath}/common/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/icheck.js"></script>
<script src="${pageContext.request.contextPath}/common/js/jquery.bxslider.min.js"></script>
<script src="${pageContext.request.contextPath}/common/js/base.js" language="javascript" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />

</head>




<script type="text/javascript">
$(document).ready(function(){
	
	$(".lev2,.lev3,.lev4,.lev5,.lev6,.lev7").sSelect({ddMaxHeight: '200px'}).next().hide();
	
	$(".sel1").sSelect({ddMaxHeight: '200'}).on('change', function () { //device
		$(".lev2,.lev3,.lev4,.lev5,.lev6,.lev7.result").hide();
		$this = $(".sel2,.sel3,.sel4,.sel5,.sel6,.sel7").off('change');
        $this.next().remove();
        $this.unbind('.sSelect');
	
        if($(this).val().length == 0) return;
        
		var url = _defaultPath+"/common/getVehicleLevSearch.do";
		var param = {lev:"2"};
		var target = "sel2";
		var listType = "1";
		var defaultVal = "S";
		
		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
			
	        $(".sel2").sSelect({ddMaxHeight: '200'}).on('change', function () { //제조사
	    		var param = {lev:"3",param1 : $(".sel2").val()};
	    		var target = "sel3";
	    		var listType = "1";
	    		var defaultVal = "S";
	    		
	    		$(".lev3,.lev4,.lev5,.lev6,.lev7,.result").hide();
    			$this = $(".sel3,.sel4,.sel5,.sel6,.sel7").off('change');
    	        $this.next().remove();
    	        $this.unbind('.sSelect');
    	        if($(this).val().length == 0) return;
    	        
	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){ 
	    			
	    	        $(".sel3").sSelect({ddMaxHeight: '200'}).on('change', function () {//차종
	    	    		var param = {lev:4,param1 : $(".sel3").val()};
	    	    		var target = "sel4";
	    	    		var listType = "1";
	    	    		var defaultVal = "S";
	    	    		
	    	    		$(".lev4,.lev5,.lev6,.lev7,.result").hide();
    	    			$this = $(".sel4,.sel5,.sel6,.sel7").off('change');
    	    	        $this.next().remove();
    	    	        $this.unbind('.sSelect');
    	    	        if($(this).val().length == 0) return;
    	    	        
	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    			
	    	    			
	    	    	        $(".sel4").sSelect({ddMaxHeight: '200'}).on('change', function () { //연식
	    	    	    		var param = {lev:5,param1 : $(".sel3").val(),param2 : $(".sel4").val()};
	    	    	    		var target = "sel5";
	    	    	    		var listType = "1";
	    	    	    		var defaultVal = "S";
	    	    	    		
	    	    	    		$(".lev5,.lev6,.lev7,.result").hide();
    	    	    			$this = $(".sel5,.sel6,.sel7").off('change');
    	    	    	        $this.next().remove();
    	    	    	        $this.unbind('.sSelect');
    	    	    	        if($(this).val().length == 0) return;
    	    	    	        
	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    			
	    	    	    	        $(".sel5").sSelect({ddMaxHeight: '200'}).on('change', function () { //헤더
	    	    	    	    		var param = {lev:6,param1 : $(".sel5").val(),param2 : $(".sel4").val()};
	    	    	    	    		var target = "sel6";
	    	    	    	    		var listType = "1";
	    	    	    	    		var defaultVal = "S";
	    	    	    	    		
	    	    	    	    		$(".lev6,.lev7,.lev8,.result").hide();
    	    	    	    			$this = $(".sel6,.sel7").off('change');
    	    	    	    	        $this.next().remove();
    	    	    	    	        $this.unbind('.sSelect');
    	    	    	    	        if($(this).val().length == 0) return;
    	    	    	    	        
	    	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    	    			
	    	    	    	    			
	    	    	    	    	        $(".sel6").sSelect({ddMaxHeight: '200'}).on('change', function () { //연료
	    	    	    	    	    		var param = {lev:7,param1 : $(".sel5").val(),param2 : $(".sel4").val()};
	    	    	    	    	    		var target = "sel7";
	    	    	    	    	    		var listType = "1";
	    	    	    	    	    		var defaultVal = "S";
	    	    	    	    	    		
	    	    	    	    	    		$(".lev7,.result").hide();
    	    	    	    	    			$this = $(".sel7").off('change');
    	    	    	    	    	        $this.next().remove();
    	    	    	    	    	        $this.unbind('.sSelect');
    	    	    	    	    	        if($(this).val().length == 0) return;
    	    	    	    	    	       
    	    	    	    	    	        
	    	    	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    	    	    			
	    	    	    	    	    	        $(".sel7").sSelect({ddMaxHeight: '200'}).on('change', function () { //배기량
    	    	    	    	    	    	        if($(this).val().length == 0) return;
	    	    	    	    	    	    		var param = {
	    	    	    	    	    	    				param1 : $(".sel2").val()
        	    	    	    	    	    				,param2 : $(".sel3").val()
        	    	    	    	    	    				,param3 : $(".sel4").val()
        	    	    	    	    	    				,param4 : $(".sel5").val()
        	    	    	    	    	    				,param5 : $(".sel6").val()
        	    	    	    	    	    				,param6 : 'AUTO'
        	    	    	    	    	    				,param7 : $(".sel7").val()
	    	    	    	    	    	    		};
	    	    	    	    	    	    		$JBIZ.http_post("/common/getBCMsurport.do",param,function(rtv){
	    	    	    	    	    	    			var bcm = rtv.data;
	    	    	    	    	    	    			if(bcm=="Y"){
	    	    	    	    	    	    				$(".resultImg").attr("src","${pageContext.request.contextPath}/common/images/car_com_resultY.png");
	    	    	    	    	    	    			}else{
	    	    	    	    	    	    				$(".resultImg").attr("src","${pageContext.request.contextPath}/common/images/car_com_resultN.png");
	    	    	    	    	    	    			}
	    	    	    	    	    	    			$(".result").show();
	    	    	    	    	    	    		});
	    	    	    	    	    	    	}); 
	    	    	    	    	    			$(".lev7").show();
	    	    	    	    	    		});
	    	    	    	    	    	}); 
	    	    	    	    			$(".lev6").show();
	    	    	    	    	        $(".lev7").sSelect({ddMaxHeight: '200'}).next().hide();
	    	    	    	    		});
	    	    	    	    	});	    	    	    	        
	    	    	    			$(".lev5").show();
	    	    	    	        $(".lev6,.lev7").sSelect({ddMaxHeight: '200'}).next().hide();
	    	    	    		});
	    	    	    	});
	    	    			$(".lev4").show();
	    	    	        $(".lev5,.lev6,.lev7").sSelect({ddMaxHeight: '200'}).next().hide();
	    	    		});
	    	    	});
	    			$(".lev3").show();
	    			$(".lev4,.lev5,.lev6,.lev7").sSelect({ddMaxHeight: '200'}).next().hide();
	    		});
	    	});
			$(".lev2").show();
			$(".lev3,.lev4,.lev5,.lev6,.lev7").sSelect({ddMaxHeight: '200'}).next().hide();
		});
	});
});
</script>

<body style="background: #fff">
	<div class="sub04_box1" style="width:100%;height:300px;" >		
			<div style="float:left;width:50%">
				<div class="lev1" style="text-align:right;height:50px;">
					<img src="${pageContext.request.contextPath}/common/images/car_com1.png" />
				</div>
				<div class="lev2" style="text-align:right;height:50px;display:none;">
					<img src="${pageContext.request.contextPath}/common/images/car_com2.png" />
				</div>
				<div class="lev3" style="text-align:right;height:50px;display:none;">
					<img src="${pageContext.request.contextPath}/common/images/car_com3.png" />
				</div>
				<div class="lev4" style="text-align:right;height:50px;display:none;">
					<img src="${pageContext.request.contextPath}/common/images/car_com4.png" />
				</div>
				<div class="lev5" style="text-align:right;height:50px;display:none;">
					<img src="${pageContext.request.contextPath}/common/images/car_com5.png" />
				</div>
				<div class="lev6" style="text-align:right;height:50px;display:none;">
					<img src="${pageContext.request.contextPath}/common/images/car_com6.png" />
				</div>
				<div class="lev7" style="text-align:right;height:50px;display:none;">
					<img src="${pageContext.request.contextPath}/common/images/car_com7.png" />
				</div>
			</div>						
			<div style="float:left;width:50%">
				<div class="lev1 select_type1" style="height:50px;width:200px;">
					<select class="sel1" id="sel1">
						<option value="" selected>Select</option>
						<option value="VC200" >VC-200</option>
					</select>
				</div>
				<div class="lev2 select_type1" style="height:50px;width:200px;;">
					<select class=" sel2" id="sel2"><option value="">Select</option></select>
				</div>
				<div class="lev3 select_type1" style="height:50px;width:200px;;">
					<select class=" sel3" id="sel3"><option value="">Select</option></select>
				</div>
				<div class="lev4 select_type1" style="height:50px;width:200px;;">
					<select class=" sel4" id="sel4"><option value="">Select</option></select>
				</div>
				<div class="lev5 select_type1" style="height:50px;width:200px;;">
					<select class=" sel5" id="sel5"><option value="">Select</option></select>
				</div>
				<div class="lev6 select_type1" style="height:50px;width:200px;;">
					<select class=" sel6" id="sel6"><option value="">Select</option></select>
				</div>
				<div class="lev7 select_type1" style="height:50px;width:200px;;">
					<select class="sel sel7" id="sel7"><option value="">Select</option></select>
				</div>
			</div>						
	</div>
	<div class="sub04_btn result" style="text-align:center;margin-left:0px;">
		<img scr="#none" class="resultImg" />
	</div>
</body>
</html>
