<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM06");
	initMenuSel("LM06_02");
});
</script>

<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		ViewCAR VDAS
	</div>
	<div class="sub_title7">
		Safe Vehicle Management Service for Rental and Commercial Vehicles, <span>ViewCAR VDAS</span>
	</div>
	<div class="video_area mab68">
		<img src="${pageContext.request.contextPath}/common/images/sub04_img3.jpg" alt="" />
	</div>
	<div class="sub04_box1">
		<h1>Simple Installation, von-Series</h1>
		<h2>
			Vehicle management service through OBD II<br>
*No additional work or parts required.
		</h2>
		<div><img src="${pageContext.request.contextPath}/common/images/sub04_img4.jpg" alt="" /></div>
		<div class="sub04_box1_1">
			<h1>High Quality in Low Cost</h1> 
			<h2>High Quality Service in low cost monthly fee</h2>
			<div><img src="${pageContext.request.contextPath}/common/images/sub04_img5.jpg" alt="" /></div>
		</div>
		<div class="sub04_box1_2">
			<h1>Vehicle Safety Enhancement,<br>
Increase in Work Efficiency</h1>
			<h2>
				Increase in Work efficiency through simple vehicle<br>allocation and management service while reducing<br>vehicle accident rate. 
			</h2>
		</div>
	</div>
	<div class="sub04_btn sub04_btn2">	
		<a id="LM05_03" data-page="/solution/vdas" class="callPage">More Information  ></a><a href="http://vdas.viewcar.co.kr/vdas">Visit ViewCAR VDAS site  ></a>
	</div>
</div>