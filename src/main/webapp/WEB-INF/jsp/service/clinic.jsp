<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM06");
	initMenuSel("LM06_03");
});
</script>

<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			ViewCAR Clinic
		</div>
		<div class="sub_title7">
			Vehicle Repair Shop Must-have, <span>ViewCAR Clinic<span>
		</div>
		<div class="video_area">
			<img src="${pageContext.request.contextPath}/common/images/sub04_img6.jpg" alt="" />
		</div>
		<div class="sub04_box1">
			<h1>Vehicle Condition Check in real time</h1>
			<h2>
				Check Vehicle condition and consumable replacement<br> based on mileage with simple installation of von-Series.
			</h2>
			<div><img src="${pageContext.request.contextPath}/common/images/sub04_img7.jpg" alt="" /></div>
			<h2>
				<span class="s1">※ Support customer unused device.</span>
			</h2>
			<div class="sub04_box1_1 matm90">
				<h1>Customer Care through Smartphone</h1>
				<h2>
					Schedule a maintenance reservation and provide advice<br>to customers through MyCarClinic Application. 
				</h2>
				<div><img src="${pageContext.request.contextPath}/common/images/sub04_img8.jpg" alt="" /></div>
			</div>
			<div class="sub04_box1_2">
				<h1>Bundle of essential maintenance service</h1>
				<h2>
					Provide web cloud based customized solution for<br> management and record
				</h2>
			</div>
		</div>
		<div class="sub04_btn sub04_btn2">
			<a id="LM05_04" data-page="/solution/clinic" class="callPage">More Information  ></a><a href="http://clinic.viewcar.co.kr/">Visit ViewCAR Clinic site  ></a>			
		</div>
	</div>
