<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	$(".lev2,.lev3,.lev4,.lev5,.lev6,.lev7,.lev8").sSelect({ddMaxHeight: '200px'}).next().hide();
	
	$(".sel1").sSelect({ddMaxHeight: '200'}).on('change', function () { //device
		$(".lev2,.lev3,.lev4,.lev5,.lev6,.lev7,.lev8,.result").hide();
		$this = $(".sel2,.sel3,.sel4,.sel5,.sel6,.sel7,.sel8").off('change');
        $this.next().remove();
        $this.unbind('.sSelect');
	
        if($(this).val().length == 0) return;
        
		var url = _defaultPath+"/common/getVehicleLevSearch.do";
		var param = {lev:"2"};
		var target = "sel2";
		var listType = "1";
		var defaultVal = "S";
		
		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
			
	        $(".sel2").sSelect({ddMaxHeight: '200'}).on('change', function () { //제조사
	    		var param = {lev:"3",param1 : $(".sel2").val()};
	    		var target = "sel3";
	    		var listType = "1";
	    		var defaultVal = "S";
	    		
	    		$(".lev3,.lev4,.lev5,.lev6,.lev7,.lev8,.result").hide();
    			$this = $(".sel3,.sel4,.sel5,.sel6,.sel7,.sel8").off('change');
    	        $this.next().remove();
    	        $this.unbind('.sSelect');
    	        if($(this).val().length == 0) return;
    	        
	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){ 
	    			
	    	        $(".sel3").sSelect({ddMaxHeight: '200'}).on('change', function () {//차종
	    	    		var param = {lev:4,param1 : $(".sel3").val()};
	    	    		var target = "sel4";
	    	    		var listType = "1";
	    	    		var defaultVal = "S";
	    	    		
	    	    		$(".lev4,.lev5,.lev6,.lev7,.lev8,.result").hide();
    	    			$this = $(".sel4,.sel5,.sel6,.sel7,.sel8").off('change');
    	    	        $this.next().remove();
    	    	        $this.unbind('.sSelect');
    	    	        if($(this).val().length == 0) return;
    	    	        
	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    			
	    	    			
	    	    	        $(".sel4").sSelect({ddMaxHeight: '200'}).on('change', function () { //연식
	    	    	    		var param = {lev:5,param1 : $(".sel3").val(),param2 : $(".sel4").val()};
	    	    	    		var target = "sel5";
	    	    	    		var listType = "1";
	    	    	    		var defaultVal = "S";
	    	    	    		
	    	    	    		$(".lev5,.lev6,.lev7,.lev8,.result").hide();
    	    	    			$this = $(".sel5,.sel6,.sel7,.sel8").off('change');
    	    	    	        $this.next().remove();
    	    	    	        $this.unbind('.sSelect');
    	    	    	        if($(this).val().length == 0) return;
    	    	    	        
	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    			
	    	    	    	        $(".sel5").sSelect({ddMaxHeight: '200'}).on('change', function () { //헤더
	    	    	    	    		var param = {lev:6,param1 : $(".sel5").val(),param2 : $(".sel4").val()};
	    	    	    	    		var target = "sel6";
	    	    	    	    		var listType = "1";
	    	    	    	    		var defaultVal = "S";
	    	    	    	    		
	    	    	    	    		$(".lev6,.lev7,.lev8,.result").hide();
    	    	    	    			$this = $(".sel6,.sel7,.sel8").off('change');
    	    	    	    	        $this.next().remove();
    	    	    	    	        $this.unbind('.sSelect');
    	    	    	    	        if($(this).val().length == 0) return;
    	    	    	    	        
	    	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    	    			
	    	    	    	    			
	    	    	    	    	        $(".sel6").sSelect({ddMaxHeight: '200'}).on('change', function () { //연료
	    	    	    	    	    		var param = {lev:7,param1 : $(".sel5").val(),param2 : $(".sel4").val()};
	    	    	    	    	    		var target = "sel7";
	    	    	    	    	    		var listType = "1";
	    	    	    	    	    		var defaultVal = "S";
	    	    	    	    	    		
	    	    	    	    	    		$(".lev7,.lev8,.result").hide();
    	    	    	    	    			$this = $(".sel7,.sel8").off('change');
    	    	    	    	    	        $this.next().remove();
    	    	    	    	    	        $this.unbind('.sSelect');
    	    	    	    	    	        if($(this).val().length == 0) return;
    	    	    	    	    	       
    	    	    	    	    	        
	    	    	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    	    	    			
	    	    	    	    	    	        $(".sel7").sSelect({ddMaxHeight: '200'}).on('change', function () { //변속기
	    	    	    	    	    	    		var param = {lev:8,param1 : $(".sel5").val(),param2 : $(".sel4").val()};
	    	    	    	    	    	    		var target = "sel8";
	    	    	    	    	    	    		var listType = "1";
	    	    	    	    	    	    		var defaultVal = "S";
	    	    	    	    	    	    		
	    	    	    	    	    	    		$(".lev8,.result").hide();
    	    	    	    	    	    			$this = $(".sel8").off('change');
    	    	    	    	    	    	        $this.next().remove();
    	    	    	    	    	    	        $this.unbind('.sSelect');
    	    	    	    	    	    	        
    	    	    	    	    	    	        if($(this).val().length == 0) return;
    	    	    	    	    	    	        
	    	    	    	    	    	    		ajaxCreateSelect(url,param,target,listType,defaultVal,"",function(){
	    	    	    	    	    	    			
	    	    	    	    	    	    	        $(".sel8").sSelect({ddMaxHeight: '200'}).on('change', function () { //배기량
	    	    	    	    	    	    	        	if($(this).val().length == 0) return;
	    	    	    	    	    	    	    		var param = {
	    	    	    	    	    	    	    				param1 : $(".sel2").val()
		        	    	    	    	    	    				,param2 : $(".sel3").val()
		        	    	    	    	    	    				,param3 : $(".sel4").val()
		        	    	    	    	    	    				,param4 : $(".sel5").val()
		        	    	    	    	    	    				,param5 : $(".sel6").val()
		        	    	    	    	    	    				,param6 : $(".sel7").val()
		        	    	    	    	    	    				,param7 : $(".sel8").val()
	    	    	    	    	    	    	    		};
	    	    	    	    	    	    	    		$JBIZ.http_post("/common/getBCMsurport.do",param,function(rtv){
	    	    	    	    	    	    	    			var bcm = rtv.data;
	    	    	    	    	    	    	    			if(bcm=="Y"){
	    	    	    	    	    	    	    				$(".result").html("도어개폐 및 차량 컨트롤을 지원합니다.");
	    	    	    	    	    	    	    			}else{
	    	    	    	    	    	    	    				$(".result").html("차량 컨트롤을 지원하지 않습니다.<br/>(VC-300)모델에서 가능합니다.");
	    	    	    	    	    	    	    			}
	    	    	    	    	    	    	    			$(".result").show();
	    	    	    	    	    	    	    		});
	    	    	    	    	    	    	        }); 
	    	    	    	    	    	    			$(".lev8").show().next().hide();
	    	    	    	    	    	    		});
	    	    	    	    	    	    	}); 
	    	    	    	    	    			$(".lev7").show();
	    	    	    	    	    	        $(".lev8").sSelect({ddMaxHeight: '200'}).next().hide();
	    	    	    	    	    		});
	    	    	    	    	    	}); 
	    	    	    	    			$(".lev6").show();
	    	    	    	    	        $(".lev7,.lev8").sSelect({ddMaxHeight: '200'}).next().hide();
	    	    	    	    		});
	    	    	    	    	});	    	    	    	        
	    	    	    			$(".lev5").show();
	    	    	    	        $(".lev6,.lev7,.lev8").sSelect({ddMaxHeight: '200'}).next().hide();
	    	    	    		});
	    	    	    	});
	    	    			$(".lev4").show();
	    	    	        $(".lev5,.lev6,.lev7,.lev8").sSelect({ddMaxHeight: '200'}).next().hide();
	    	    		});
	    	    	});
	    			$(".lev3").show();
	    			$(".lev4,.lev5,.lev6,.lev7,.lev8").sSelect({ddMaxHeight: '200'}).next().hide();
	    		});
	    	});
			$(".lev2").show();
			$(".lev3,.lev4,.lev5,.lev6,.lev7,.lev8").sSelect({ddMaxHeight: '200'}).next().hide();
		});
	});
});
</script>

<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		지원차종
	</div>
	<div class="sub_title7">
		뭔가 할말 <span>없나요?</span>
	</div>
	<div class="video_area mab68">
		<img src="/jbiz/common/images/img_carCompatibility.png" style="width:1220px;height:500px;" />
	</div>
	<div class="sub04_box1" style="width:100%;height:350px;" >		
			<div style="float:left;width:50%">
				<div class="lev1" style="text-align:right;height:50px;">단말기 :</div>
				<div class="lev2" style="text-align:right;height:50px;display:none;">제조사 :</div>
				<div class="lev3" style="text-align:right;height:50px;display:none;">차종 :</div>
				<div class="lev4" style="text-align:right;height:50px;display:none;">연식 :</div>
				<div class="lev5" style="text-align:right;height:50px;display:none;">모델 :</div>
				<div class="lev6" style="text-align:right;height:50px;display:none;">연료 :</div>
				<div class="lev7" style="text-align:right;height:50px;display:none;">변속기 :</div>
				<div class="lev8" style="text-align:right;height:50px;display:none;">배기량 :</div>
			</div>						
			<div style="float:left;width:50%">
				<div class="lev1 select_type1" style="height:50px;width:200px;">
					<select class="sel1" id="sel1">
						<option value="" selected>선택</option>
						<option value="VC200" >VC-200</option>
					</select>
				</div>
				<div class="lev2 select_type1" style="height:50px;width:200px;;">
					<select class=" sel2" id="sel2"><option value="">선택</option></select>
				</div>
				<div class="lev3 select_type1" style="height:50px;width:200px;;">
					<select class=" sel3" id="sel3"><option value="">선택</option></select>
				</div>
				<div class="lev4 select_type1" style="height:50px;width:200px;;">
					<select class=" sel4" id="sel4"><option value="">선택</option></select>
				</div>
				<div class="lev5 select_type1" style="height:50px;width:200px;;">
					<select class=" sel5" id="sel5"><option value="">선택</option></select>
				</div>
				<div class="lev6 select_type1" style="height:50px;width:200px;;">
					<select class=" sel6" id="sel6"><option value="">선택</option></select>
				</div>
				<div class="lev7 select_type1" style="height:50px;width:200px;;">
					<select class=" sel7" id="sel7"><option value="">선택</option></select>
				</div>
				<div class="lev8 select_type1" style="height:50px;width:200px;;">
					<select class="sel sel8" id="sel8"><option value="">선택</option></select>
				</div>
			</div>						
	</div>
	<div class="sub04_btn result" style="text-align:center;margin-left:0px;"></div>
