<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM06");
	initMenuSel("LM06_01");
});
</script>

<div class="sub_layout">
	<div class="sub_title1">
		<span></span>
		ViewCAR
	</div>
	<div class="sub_title7">
		Smart Driving Solution in my hand, <span>ViewCAR</span>
	</div>
	<div class="video_area mab68">
		<iframe width="800" height="452" src="https://www.youtube.com/embed/3zJlbkJMSjw" frameborder="0" allowfullscreen></iframe>		
	</div>
	<div class="sub04_box1">
		<h1>OBD Devices, VON Series </h1>
		<h2>Experience Connected Car service through OBD II</h2>
		<div><img src="${pageContext.request.contextPath}/common/images/sub04_img1.jpg" alt="" /></div>
		<div class="sub04_box1_1">
			<h1>Connected Car App, ViewCAR</h1>
			<h2>Experience driving behavior monitoring and vehicle management service through your smartphone.</h2>
			<p>
				Download is available at google play!
				<a href="https://play.google.com/store/apps/details?id=kr.co.infinityplus.viewcar&hl=ko"><img src="${pageContext.request.contextPath}/common/images/btn_down2.jpg" alt="" /></a>
			</p>
			<div><img src="${pageContext.request.contextPath}/common/images/sub04_img2.jpg" alt="" /></div>
		</div>
		<div class="sub04_box1_2">
			<h1>Communication between 
Smartphone and Vehicle</h1>
			<h2>
				Provides useful information (Sensor, Trip,<br>Trouble) and service through deep-learning<br>A.I. vehicle platform.
			</h2>
		</div>
	</div>
	<div class="sub04_btn">
		<a href="http://www.viewcar.co.kr/viewcar-clinic/wwwroot/viewCarFeature.do#pos_top">More Information ></a><a href="http://www.viewcar.co.kr/viewcar-clinic/wwwroot/index.do">Visit ViewCAR site ></a>
	</div>
</div>