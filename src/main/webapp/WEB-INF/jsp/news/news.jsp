<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("LM03");
	initMenuSel("LM03_01");
	
});

var pageUrl =  "/news/news";
$(document).ready(function(){

	
		
	$(".menu").each(function(){
		if($(this).data("menu") == "${reqVo.searchContentType}") $(this).addClass("on");
	})
	
		
	$("#btn_search").on("click",function(){
		var p = pageUrl;
		var f = "frm";
		var frmParam = $("#"+f).serializeArray();
		var param = {};
		for(var i= 0 ; i < frmParam.length ; i++){
			param[frmParam[i].name] = frmParam[i].value; 
		}
		
		param.callPage = p;
		var url = "/index.do";
		$JBIZ.instance_post(url,param);
	});
	
	$(".menu").on("click",function(){
		var menu = $(this).data("menu");
		$("#frm input[name=searchContentType]").val(menu);
		
		$("#btn_search").trigger("click");		
	});
});
</script>
<div class="sub_layout">
		<div class="sub_title1">
			<span></span>
			NEWS
		</div>
		<div class="sub_title3">
			Updated news from JastecM
		</div>
<div class="tabbable">
	<ul class="tabs">	
		<div class="sub_tab mat53">
			<a data-menu="" class="menu">ALL</a><a data-menu="1" class="menu">NEWS</a><a data-menu="2" class="menu">PRESS</a>
		</div>
	</ul>	
		<table id="table" class="table1 table1_faq">
			<colgroup>
				<col width="80" />
				<col width="*" />
				<col width="100" />
			</colgroup>
			<thead>
				<tr>
					<th>No.</th>
					<th>Title</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${fn:length(rtvList) eq 0}">
					<tr class="f">
						<td colspan="2" style='text-align:center'>No search results.</td>
					</tr>
				</c:if> 
				<c:if test="${fn:length(rtvList) > 0 }">
					<c:forEach var="vo" items="${rtvList}" varStatus="i">
						<tr class="f">
							<th>${vo.contentTypeNm }</th>
							<td style='text-align:center'>${vo.title }</td>
							<th>
							<fmt:parseDate var="d" value="${vo.regDate }" pattern="yyyy-MM-dd HH:mm:ss.S"/>
							<fmt:formatDate value="${d}" pattern="yyyy-MM-dd"/>
							</th> 
						</tr>
						<tr class="a">
							<th></th>
							<td style='text-align:center'>
								${vo.contents }
							</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
	<form id="frm">
		<input type="hidden" name="searchBoardType" value="" />
		<input type="hidden" name="searchContentType" value="${reqVo.searchContentType}" />
		<div class="search_box mat53">
			<input type="text" name="searchText" placeholder="Title, Contents, Type" value="${reqVo.searchText}"/><button id="btn_search">검색</button>
		</div>
	</form>
	<script type="text/javascript">	
		var pagingFieldData = new Array (
				{fieldName:"searchBoardType",fieldValue:"${reqVo.searchBoardType}"}
				,{fieldName:"searchContentType",fieldValue:"${reqVo.searchContentType}"}
				,{fieldName:"searchText",fieldValue:"${reqVo.searchText}"}
				,{fieldName:"callPage",fieldValue:pageUrl}
		);
		var pagingProperty = {
			urlPath: "${pageContext.request.contextPath}/index.do" 	//required
		}	
		</script>
		<%@ include file="/WEB-INF/jsp/common/PagingNavi.jsp"%>
	</div>
</div>