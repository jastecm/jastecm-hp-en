<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="자동차 IOT 디바이스(통신형 OBD, 블루투스 OBD) 개발·제조">
<meta property="og:title" content="자스텍M">
<meta property="og:description" content="자동차 IOT 디바이스(통신형 OBD, 블루투스 OBD) 개발·제조">
<title><tiles:insertAttribute name="title" ignore="true" /></title>

<tiles:insertAttribute name="css" ignore="true" />

<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script charset="utf-8" src="${pageContext.request.contextPath}/common/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/icheck.js"></script>
</head>
<body>


<div class="wrap">
	<tiles:insertAttribute name="left"/>
	
	<tiles:insertAttribute name="contents"/>			
	
	<tiles:insertAttribute name="footer"/> 
</div>

<script src="${pageContext.request.contextPath}/common/js/jquery.bxslider.min.js"></script>
<script src="${pageContext.request.contextPath}/common/js/base.js" language="javascript" type="text/javascript"></script>

</body>
</html>