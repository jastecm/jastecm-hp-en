<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
}
</script>
<div class="left_menu">		
		<div class="lan">
			<a href="http://www.jastecm.com/jbiz/index.do">KOREAN</a><a class="on" href="${pageContext.request.contextPath}/index.do">ENGLISH</a>
		</div>
		<div class="logo"><a href="${pageContext.request.contextPath}/index.do" ><img src="${pageContext.request.contextPath}/common/images/logo.jpg" alt="" /></a></div>
		<ul class="menu">
			<li id="LM01">
				<a href="${pageContext.request.contextPath}/index.do" class="depth1 callPage">JASTECM</a>
			</li>
			
			<li id="LM02">
				<a href="${pageContext.request.contextPath}/corporate/vision.do" class="depth1 callPage">COMPANY</a>
				<div class="depth2">
					<a id="LM02_01" data-page="/corporate/vision" class="callPage">ㆍ Vision</a>
					<a id="LM02_02" data-page="/corporate/history" class="callPage">ㆍ History</a>
					<a id="LM02_03" data-page="/corporate/ceomsg" class="callPage">ㆍ CEO</a>
					<a id="LM02_04" data-page="/corporate/business" class="callPage">ㆍ Business</a>
					<a id="LM02_05" data-page="/corporate/info" class="callPage">ㆍ Company</a>
					<a id="LM02_06" data-page="/corporate/tech" class="callPage">ㆍ Certification</a>
					<a id="LM02_07" data-page="/corporate/partner" class="callPage">ㆍ Partner</a>
					<a id="LM02_08" data-page="/corporate/partnership" class="callPage">ㆍ Proposal</a>
				</div>
			</li>
			
			
			<li id="LM03">
				<a href="${pageContext.request.contextPath}/news/news.do" class="depth1 callPage">NEWS</a>
				<div class="depth2">
				<a id="LM03_01" data-page="/news/news" class="callPage">ㆍ News</a>
				</div>
			</li>
			
			<li id="LM04">
				<a href="${pageContext.request.contextPath}/product/tech.do" class="depth1 callPage">PRODUCT</a>
				<div class="depth2">
					<a id="LM04_01" data-page="/product/tech" class="callPage">ㆍ Tech</a>
					<a id="LM04_02" data-page="/product/type" class="callPage">ㆍ IoT device
					
				</div>
			</li>
			
			<li id="LM05">
				<a href="${pageContext.request.contextPath}/solution/info.do" class="depth1 callPage">SOLUTION</a>
				<div class="depth2">
					<a id="LM05_01" data-page="/solution/info" class="callPage">ㆍ Solution</a>
					<a id="LM05_02" data-page="/solution/apps" class="callPage">ㆍ App</a>
					<a id="LM05_03" data-page="/solution/vdas" class="callPage">ㆍ VDAS</a>
					<a id="LM05_04" data-page="/solution/clinic" class="callPage">ㆍ VClinic</a>
					<a id="LM05_05" data-page="/solution/share" class="callPage">ㆍ VShare</a>
					<a id="LM05_06" data-page="/solution/scoring" class="callPage">ㆍ VScoring</a>
					<a id="LM05_07" data-page="/solution/call" class="callPage">ㆍ VCall</a>
					<a id="LM05_08" data-page="/solution/comm" class="callPage">ㆍ VComm</a>
				</div>
			</li>
			
			<li id="LM06">
				<a href="${pageContext.request.contextPath}/service/viewcar.do" class="depth1 callPage">SERVICE</a>
				<div class="depth2">
					<a id="LM06_01" data-page="/service/viewcar" class="callPage">ㆍ ViewCAR</a>
					<a id="LM06_02" data-page="/service/vdas" class="callPage">ㆍ VDAS</a>
					<a id="LM06_03" data-page="/service/clinic" class="callPage">ㆍ Clinic</a>
					<a id="LM06_04" data-page="/service/carCompatibility" class="callPage">ㆍ Car Compatibility</a>
				</div>
			</li>
		
			
			<li id="LM07">
				<a href="${pageContext.request.contextPath}/employ/corp.do" class="depth1 callPage">RECRUIT</a>
				<div class="depth2">
					<a id="LM07_01" data-page="/employ/corp" class="callPage">ㆍ Company</a>
					<a id="LM07_02" data-page="/employ/ceomsg" class="callPage">ㆍ Message</a>
					<a id="LM07_03" data-page="/employ/today" class="callPage">ㆍ Recent event</a>
					<a id="LM07_04" data-page="/employ/noti" class="callPage">ㆍ Recruit</a>
				</div>
			</li> 
			<li id="LM08">
				<a href="${pageContext.request.contextPath}/cs/faq.do" class="depth1 callPage">CUSTOMER</a>
				<div class="depth2">
					<a id="LM08_01" data-page="/cs/faq" class="callPage">ㆍ FAQ</a>
					<a id="LM08_02" data-page="/cs/inquiry" class="callPage">ㆍ 1:1 inquiry</a>
					<a id="LM08_03" data-page="/cs/download" class="callPage">ㆍ Download</a>
				</div>
			</li>
		</ul>
		<ul class="site">
			<li>RELATION SITE</li>
			<li>
				<a href="http://viewcar.net/">ViewCAR</a>
				<a href="http://vdaspro.viewcar.co.kr">ViewCAR FMS</a>
				<a href="http://jastec.co.kr">Jastec</a>
			</li>
		</ul>
	</div>