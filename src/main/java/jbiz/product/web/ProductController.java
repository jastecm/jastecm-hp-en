package jbiz.product.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.BasicController;

import jbiz.vo.ParamVO;

@Controller
public class ProductController extends BasicController{
private Log logger = LogFactory.getLog(getClass());	
	
	
	@RequestMapping(value="/product/tech.do")
	public String tech(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/tech";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/product/type.do")
	public String type(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/type";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/product/VonSTD.do")
	public String VonSTD(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/VonSTD";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/product/Notyet.do")
	public String Notyet(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/Notyet";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/product/VonS3.do")
	public String VonS3(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/VonS3";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/product/VonS21.do")
	public String VonS21(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/VonS21";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/product/VonS2.do")
	public String VonS2(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/VonS2";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/product/VonG2.do")
	public String VonG2(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/VonG2";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/product/VonG.do")
	public String VonG(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/VonG";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	@RequestMapping(value="/product/VonF.do")
	public String VonF(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo ) throws Exception  {
		
		String rtvPage = "/product/VonF";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	
	
}
