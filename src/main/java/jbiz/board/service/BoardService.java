package jbiz.board.service;

import java.util.List;

import jbiz.vo.BoardVO;





public interface BoardService {
	
	public int getBoardListCnt(BoardVO vo) throws Exception;
	public List<BoardVO> getBoardList(BoardVO vo) throws Exception;
}
	

