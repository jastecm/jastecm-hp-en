package jbiz.board.dao;

import java.util.List;

import jbiz.vo.BoardVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

@Repository("boardDao")
@SuppressWarnings({ "unchecked", "deprecation" })
public class BoardDAO {
	
	@SuppressWarnings("unused")
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	private SqlMapClientTemplate sqlMapClientTemplate;
	
	public int getBoardListCnt(BoardVO vo) throws Exception{
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : board.getBoardListCnt ]");
		return (Integer)sqlMapClientTemplate.queryForObject("board.getBoardListCnt",vo);
	}
	public List<BoardVO> getBoardList(BoardVO vo) throws Exception{
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : board.getBoardList ]");
		return (List<BoardVO>)sqlMapClientTemplate.queryForList("board.getBoardList",vo);
	}
}
