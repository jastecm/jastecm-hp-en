package jbiz.main.web;

import javax.servlet.http.HttpServletRequest;

import jbiz.vo.ForwardVO;
import jbiz.vo.ParamVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.common.BasicController;
import com.util.string.StrUtil;


/*
 * @Class name : MainController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Controller
public class MainController extends BasicController{
		
	private Log logger = LogFactory.getLog(getClass());	
	
	
	/**
	 * 메인페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/index.do")
	public String mainPage(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ForwardVO reqVo) throws Exception  {
		
		String rtvPage = "/main/page";
		
		try {
			
			if(!StrUtil.isNullToEmpty(reqVo.getCallPage())) rtvPage = "forward:"+reqVo.getCallPage()+".do";
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	
	
}

