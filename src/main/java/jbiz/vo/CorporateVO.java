package jbiz.vo;


public class CorporateVO  {
	
	private String name;
	private String partnershipType;
	private String contents;
	private String file;
	private String fileNm;
	private String person;
	private String corpo;
	private String personType;
	private String phone;
	private String email;
	private String file2;
	private String file2Nm;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPartnershipType() {
		return partnershipType;
	}
	public void setPartnershipType(String partnershipType) {
		this.partnershipType = partnershipType;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getFileNm() {
		return fileNm;
	}
	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}
	public String getPerson() {
		return person;
	}
	public void setPerson(String person) {
		this.person = person;
	}
	public String getCorpo() {
		return corpo;
	}
	public void setCorpo(String corpo) {
		this.corpo = corpo;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFile2() {
		return file2;
	}
	public void setFile2(String file2) {
		this.file2 = file2;
	}
	public String getFile2Nm() {
		return file2Nm;
	}
	public void setFile2Nm(String file2Nm) {
		this.file2Nm = file2Nm;
	}
	
	
}
