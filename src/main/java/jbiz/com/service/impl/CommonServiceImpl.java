package jbiz.com.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import jbiz.com.dao.CommonDao;
import jbiz.com.service.CommonService;
import jbiz.vo.FileVO;
import jbiz.vo.ParamVO;

import org.apache.commons.fileupload.FileItem;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("commonService")
@Transactional
public class CommonServiceImpl implements CommonService {
	@Resource(name = "commonDao")
	private CommonDao dao;
	
	public String createMd5Uuid() throws Exception{
		return dao.createMd5Uuid();
	}
	
	public String insertFileUpload(FileVO vo) throws Exception{
		
		String uuid = createMd5Uuid();
		vo.setUuid(uuid);
		dao.insertFileUpload(vo);
		return uuid;
	}
	
	public FileVO getFile(String vo) throws Exception{
		return dao.getFile(vo);
	}
	
	/**
     * 파일업로드 리퀘스트 파싱
     * @param iter
     * @param fMidir
     * @return
     */
	public HashMap<String,String> getFileUpload(Iterator iter, File fMidir) {
		HashMap file_map = new HashMap();
		String file_name = "";
		List fileList = new ArrayList();
		List fileNmList = new ArrayList();
		List file2List = new ArrayList();
		List file2NmList = new ArrayList();
		try{
			while(iter.hasNext()){
				FileItem item = (FileItem)iter.next();
				//파일이 아닌경우 (필드 파라메터)
				if(item.isFormField()){
					String key = item.getFieldName();    
					//한글 꺠지는거 방지 utf-8로 재설정
					String value = item.getString("utf-8");
					//System.out.println("key=" + key + " value=" + value);
					file_map.put(key, value);
				
				//파일처리 + 중복처리	
				}else{
					FileVO vo = new FileVO();
					
					String key = item.getFieldName();
					
					//한글 계속 깨짐 
					String tempfileName = new String(item.getName().getBytes("euc-kr"),"KSC5601");
					String orgFileName = "";
					String saveFileName = "";
					
					//파일이름이 존재하고 업로드할 파일이 존재한다면 업로드 시작
					if(!tempfileName.equals("") && tempfileName != null){
						//System.out.println("filefieldname=" + key + " fileName=" + tempfileName);
						//파일명 추출
						int idx = tempfileName.lastIndexOf(System.getProperty("file.separator"));
						
						String fileSize = String.valueOf(item.getSize());
						vo.setFileSize(fileSize);
						
						//파일 풀 name(확장자포함)
						orgFileName = tempfileName.substring(idx + 1);
						vo.setOrgFileNm(orgFileName);
						
						//확장자 추출
						String fileExt = orgFileName.substring(orgFileName.lastIndexOf("."));
						vo.setFileExt(fileExt);
						
						//파일명 추출
						orgFileName = orgFileName.substring(0,orgFileName.lastIndexOf("."));
						
						//저장될 파일명
						saveFileName = orgFileName+fileExt;
						
						//중복 파일 처리
						File save_file = new File(fMidir.getPath(),saveFileName);
						//파일이 존재한다면
						int i = 0;
						while(save_file.exists()){
							i++;
							//fileName_i
							saveFileName = orgFileName+"_"+i+fileExt;
							save_file = new File(fMidir.getPath(),saveFileName);
						}
							
						vo.setFilePath(fMidir.getPath());
						vo.setSaveFileNm(saveFileName);						
						item.write(save_file);
						//중복 파일 처리 End
						
						String uuid = insertFileUpload(vo);
						
						if(key.equals("file")){
							fileList.add(uuid);
							fileNmList.add(orgFileName+fileExt);
							file_map.put(key, fileList);
							file_map.put(key+"Nm", fileNmList);
						}
						else{
							file2List.add(uuid);
							file2NmList.add(orgFileName+fileExt);
							file_map.put(key, file2List);
							file_map.put(key+"Nm", file2NmList);
						}
						
						
						
					}else{
						file_map.put(key, null);
					}//End Of FileName if
				}                           
			}                
		}catch(Exception e){
		}finally{}
		
		return file_map;
	}

	@Override
	public List<String> getManufacture(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getManufacture(vo);
	}

	@Override
	public List<String> getModelMaster(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getModelMaster(vo);
	}

	@Override
	public List<String> getYear(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getYear(vo);
	}

	@Override
	public List<String> getModelHeader(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getModelHeader(vo);
	}

	@Override
	public List<String> getFuel(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getFuel(vo);
	}

	@Override
	public List<String> getTransmission(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getTransmission(vo);
	}

	@Override
	public List<String> getVolume(ParamVO vo) throws Exception {
		// TODO Auto-generated method stub
		return dao.getVolume(vo);
	}
	
	public String getBCMsurport(ParamVO vo) throws Exception{
		return dao.getBCMsurport(vo);
	}
}