package jbiz.com.service;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import jbiz.vo.FileVO;
import jbiz.vo.ParamVO;




public interface CommonService {
	public String insertFileUpload(FileVO vo) throws Exception;
	public FileVO getFile(String vo) throws Exception;
	public HashMap<String,String> getFileUpload(Iterator iter, File fMidir);
	
	public List<String> getManufacture (ParamVO vo) throws Exception;
	public List<String> getModelMaster(ParamVO vo) throws Exception;
	public List<String> getYear(ParamVO vo) throws Exception;
	public List<String> getModelHeader(ParamVO vo) throws Exception;
	public List<String> getFuel(ParamVO vo) throws Exception;
	public List<String> getTransmission(ParamVO vo) throws Exception;
	public List<String> getVolume(ParamVO vo) throws Exception;
	
	public String getBCMsurport(ParamVO vo) throws Exception;
	
}
	

