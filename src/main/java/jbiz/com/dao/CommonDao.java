package jbiz.com.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jbiz.vo.FileVO;
import jbiz.vo.ParamVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Repository;

@Repository("commonDao")
@SuppressWarnings({ "unchecked", "deprecation" })
public class CommonDao {
	
	@SuppressWarnings("unused")
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	private SqlMapClientTemplate sqlMapClientTemplate;
	
	public String createMd5Uuid() throws Exception{
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : file.createMd5Uuid ]");
		return (String)sqlMapClientTemplate.queryForObject("file.createMd5Uuid");
	}
	public void insertFileUpload(FileVO vo) throws Exception{
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : file.insertFileUpload ]");
		sqlMapClientTemplate.insert("file.insertFileUpload",vo);
	}
	public FileVO getFile(String vo) throws Exception{
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : file.getFile ]");
		return (FileVO)sqlMapClientTemplate.queryForObject("file.getFile",vo);
	}
	public List<String> getManufacture(ParamVO vo) throws Exception {
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : common.getManufacture ]");
		return (List<String>)sqlMapClientTemplate.queryForList("common.getManufacture",vo);
	}
	public List<String> getModelMaster(ParamVO vo) throws Exception {
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : common.getModelMaster ]");
		return (List<String>)sqlMapClientTemplate.queryForList("common.getModelMaster",vo);
	}
	public List<String> getYear(ParamVO vo) throws Exception {
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : common.getYear ]");
		return (List<String>)sqlMapClientTemplate.queryForList("common.getYear",vo);
	}
	public List<String> getModelHeader(ParamVO vo) throws Exception {
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : common.getModelHeader ]");
		return (List<String>)sqlMapClientTemplate.queryForList("common.getModelHeader",vo);
	}
	public List<String> getFuel(ParamVO vo) throws Exception {
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : common.getFuel ]");
		return (List<String>)sqlMapClientTemplate.queryForList("common.getFuel",vo);
	}
	public List<String> getTransmission(ParamVO vo) throws Exception {
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : common.getTransmission ]");
		return (List<String>)sqlMapClientTemplate.queryForList("common.getTransmission",vo);
	}
	public List<String> getVolume(ParamVO vo) throws Exception {
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : common.getVolume ]");
		return (List<String>)sqlMapClientTemplate.queryForList("common.getVolume",vo);
	}
	public String getBCMsurport(ParamVO vo) throws Exception {
		if (logger.isDebugEnabled()) logger.debug("[SQL ID : common.getBCMsurport ]");
		return (String)sqlMapClientTemplate.queryForObject("common.getBCMsurport",vo);
	}
}
