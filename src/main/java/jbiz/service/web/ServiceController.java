package jbiz.service.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jbiz.com.service.CommonService;
import jbiz.vo.ParamVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.BasicController;


/*
 * @Class name : MainController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Controller
public class ServiceController extends BasicController{
		
	private Log logger = LogFactory.getLog(getClass());	
	
	@Resource(name = "commonService")
	private CommonService commonService;
	
	@RequestMapping(value="/service/viewcar.do")
	public String viewcar(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/viewcar";
		
		try {
			
			model.addAttribute("reqVo", reqVo);
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/vdas.do")
	public String vdas(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/vdas";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/clinic.do")
	public String clinic(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/clinic";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/carCompatibility.do")
	public String carCompatibility(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/carCompatibility";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/service/carCompatibilityFrame.do")
	public String carCompatibilityFrame(HttpServletRequest request,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "/service/carCompatibilityFrame";
		
		try {
			
	        model.addAttribute("reqVo", reqVo);
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/common/getVehicleLevSearch.do")
	public String getVehicleLevSearch(HttpServletRequest request,HttpServletResponse response,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "jsonView";
		response.setContentType("application/x-json; charset=UTF-8");
		
		try {
			
			if(reqVo.getLev().equals("2")) model.addAttribute("rtvList",commonService.getManufacture(reqVo));
			else if(reqVo.getLev().equals("3")) model.addAttribute("rtvList",commonService.getModelMaster(reqVo));
			else if(reqVo.getLev().equals("4")) model.addAttribute("rtvList",commonService.getYear(reqVo));
			else if(reqVo.getLev().equals("5")) model.addAttribute("rtvList",commonService.getModelHeader(reqVo));
			else if(reqVo.getLev().equals("6")) model.addAttribute("rtvList",commonService.getFuel(reqVo));
			else if(reqVo.getLev().equals("7")) model.addAttribute("rtvList",commonService.getVolume(reqVo));
	        
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/common/getBCMsurport.do")
	public String getBCMsurport(HttpServletRequest request,HttpServletResponse response,
    		ModelMap model, @ModelAttribute ParamVO reqVo) throws Exception  {
		
		String rtvPage = "jsonView";
		response.setContentType("application/x-json; charset=UTF-8");
		
		try {
			
			model.addAttribute("data",commonService.getBCMsurport(reqVo));
			
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
}

