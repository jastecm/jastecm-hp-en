package com.util.mail;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeUtility;

import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

/**
 *	<br>
 *
 * @author TA
 * @since J2EE 1.6
 * @see 
 */
public class EmailMessage {

	private boolean isTemplate; 
	
	private String encoding = "UTF-8";
	private EmailAddress from;
	private String fromPw;
	private EmailAddress replyTo;
	private List<EmailAddress> to = new ArrayList<EmailAddress>();
	private List<EmailAddress> cc = new ArrayList<EmailAddress>();
	private List<EmailAddress> bcc = new ArrayList<EmailAddress>();
	private String subject;

	private String contentType = TemplateConstants.TEXT.toString();
	private String templateId;
	private String templateContent;
	private String simpleContent;
	
	private Map<String, Resource> attatchs = new HashMap<String, Resource>();
	private Map<String, Resource> inlines = new HashMap<String, Resource>();
	private Map<String, File> file = new HashMap<String, File>();
	
	public String getFromPw() {
		return fromPw;
	}
	public void setFromPw(String fromPw) {
		this.fromPw = fromPw;
	}
	public Map<String, File> getAttatchsFile() {
		return file;
	}
	public void setAttatchsFile(File file) {
		this.file.put(file.getName(), file);
	}
	public String getEncoding() {
		return encoding;
	}
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}	
	// from
	public void setFrom(String email) {
		setFrom(email, null);
	}
	public void setFrom(String email, String personal) {
		Assert.notNull(email, "FROM email address must not be null");
		from = new EmailAddress(email);
		
		if( personal != null )
			from.setPersonal(personal); 
	}
	public EmailAddress getFrom() {
		return this.from;
	}
	// replyTo
	public void setReplyTo(String email) {
		setReplyTo(email, null);
	}
	public void setReplyTo(String email, String personal) {
		Assert.notNull(email, "ReplyTo email address must not be null");
		replyTo = new EmailAddress(email);
		
		if( personal != null )
			replyTo.setPersonal(personal);
	}
	public EmailAddress getReplyTo() {
		return this.replyTo;
	}
	// To
	public void addTo(String email) {
		addTo(email, null);
	}
	public void addTo(String email, String personal) {
		Assert.notNull(email, "TO email address must not be null");
		EmailAddress mailAddress = new EmailAddress(email);
		
		if( personal != null )
			mailAddress.setPersonal(personal);
		to.add(mailAddress);
	}
	public List<EmailAddress> getTo() {
		return this.to;
	}
	// CC
	public void addCc(String email) {
		addCc(email, null);
	}
	public void addCc(String email, String personal) {
		Assert.notNull(email, "CC email address must not be null");
		EmailAddress mailAddress = new EmailAddress(email);
		if( personal != null )
			mailAddress.setPersonal(personal);
		cc.add(mailAddress);
	}
	public List<EmailAddress> getCc() {
		return this.cc;
	}
	// BCC
	public void addBcc(String email) {
		addBcc(email, null);
	}
	public void addBcc(String email, String personal) {
		Assert.notNull(email, "BCC email address must not be null");
		EmailAddress mailAddress = new EmailAddress(email);
		if( personal != null )
			mailAddress.setPersonal(personal);
		bcc.add(mailAddress);
	}
	public List<EmailAddress> getBcc() {
		return this.bcc;
	}
	// Subject
	public void setSubject(String subject) {
		Assert.notNull(subject, "SUBJECT must not be null");
		this.subject = subject;
	}
	public String getSubject() {
		return this.subject;
	}
	// Content without Template
	public void setSimpleContent(String mailContent, boolean isHtml) {
		Assert.notNull(mailContent, "Mail Content must not be null");
		this.simpleContent = mailContent;
		
		if( isHtml )
			this.contentType = TemplateConstants.HTML.toString();
	}
	public String getSimpleContent() {
		return this.simpleContent;
	}
	// Content with Template
	public void setTemplateContent(String templateId, boolean isHtml) {
		setTemplateContent(templateId, null, isHtml);
	}
	
	/**
	 * Template Name, Content, HTML 여부
	 * @param templateId
	 * @param templateContent
	 * @param isHtml
	 * @throws MessagingException
	 */
	public void setTemplateContent(String templateId, String templateContent, boolean isHtml ) {
		Assert.notNull(templateId, "TemplateId must not be null");
		this.templateId = templateId;
		this.templateContent = templateContent;		
		if( isHtml )
			this.contentType = TemplateConstants.HTML.toString();
		
		// message Type
		this.isTemplate = true;
	}
	
	public String getTemplateId() {
		return this.templateId;
	}
	
	public String getTemplateContent() {
		return this.templateContent;
	}
	
	/**
	 * Template 사용 혹은 주어진 Template Content String 이용 여부를 판단하기 위한 Method
	 * @return
	 */
	public boolean hasTemplateContent() {
		boolean result = false;
		if( this.templateContent != null )
			result = true;
		
		return result;
	}
	
	public boolean isHtmlContent() {
		boolean result = false;
		if( TemplateConstants.HTML.compareTo(this.contentType) )
			result = true;
		
		return result;
	}
	
	/**
	 * Template Mail 여부
	 * @return
	 */
	public boolean isTemplate() {
		return this.isTemplate;
	}
	
	
	// Attatch
	public void addAttachment(String name, Resource resource) {
		Assert.notNull(name, "Attatchment Name must not be null");
		Assert.notNull(resource, "Attatchment Resource must not be null");
		try {
			this.attatchs.put(MimeUtility.encodeText(name), resource);
			//this.attatchs.put(MimeUtility.encodeText(name,"euc-kr","B"), resource);
		} catch (UnsupportedEncodingException e) {
			this.attatchs.put(name, resource);
		}
	}
	
	public Map<String, Resource> getAttachments() {
		return this.attatchs;
	}
	
	public boolean hasAttachment() {
		boolean result = false;
		if( this.attatchs.size() > 0 )
			result = true;
		return result;
	}
	
	// Inline
	public void addInline(String name, Resource resource) {
		Assert.notNull(name, "InLine Name must not be null");
		Assert.notNull(resource, "InLine Resource must not be null");
		this.inlines.put(name, resource);
	}
	
	public Map<String, Resource> getInlines() {
		return this.inlines;
	}
	
	public boolean hasInline() {
		boolean result = false;
		if( this.inlines.size() > 0 )
			result = true;
		return result;
	}
}
