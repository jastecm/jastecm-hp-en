package com.util.mail;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import org.springframework.web.servlet.view.velocity.VelocityConfig;

public class VelocityTemplateHelper implements TemplateHelper, InitializingBean {

	VelocityConfig config;
	
	public void setConfig(VelocityConfig config) {
		this.config = config;
	}

	public void afterPropertiesSet() throws Exception {
		Assert.notNull(config, "'config' property is required");
	}
	
	public String mergeTemplate(String templateId, Map<String, Object> values, String templateContent, String encoding) throws Exception {
		StringBuffer content = new StringBuffer();
		
		Template template = null;
		// 1. template find
		if( templateContent != null ) {
			RuntimeServices runtimeServices = RuntimeSingleton.getRuntimeServices();
			StringReader reader = new StringReader(templateContent);
			SimpleNode node = runtimeServices.parse(reader, templateId);
			
			template = new Template();
			template.setRuntimeServices(runtimeServices);
			template.setData(node);
			template.initDocument();
		}
		else {
			template = config.getVelocityEngine().getTemplate(templateId);
		}
		
		// 2. values mapping
		VelocityContext velocityContext = new VelocityContext(values);
		StringWriter result = new StringWriter();
		template.merge(velocityContext, result);

		content.append(result.toString());
		return content.toString();
	}
}
