package com.util.mail;

import java.util.Map;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.Assert;

public class SimpleEmailSender implements InitializingBean {

	private static JavaMailSenderImpl mailSender;
	
	@SuppressWarnings("static-access")
	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}
	
	public static Session getSession(){
		return mailSender.getSession();
	}
	
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(mailSender, "'mailSender' property is required");
	}
	
	public static void sendEMail(EmailMessage emailMessage) throws Exception {
		
//		mailSender.setUsername(emailMessage.getFrom().getEmail());
//		mailSender.setPassword(emailMessage.getFromPw());
		emailMessage.setFrom(mailSender.getUsername()); 
		
		// 1. EmailMessage valid check
		// Required : From, To, Subject, Template ID
		EmailMessageHelper.mailValidation(emailMessage);
		// create MimeMessage
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		// create MimeMessageHelper ( encoding )
		MimeMessageHelper helper = EmailMessageHelper.createMimeMessageHelper(emailMessage, mimeMessage);
		// set From : REQUIRED 
		EmailMessageHelper.setFrom(emailMessage, helper);
		// set ReplyTo
		EmailMessageHelper.setReplyTo(emailMessage, helper);
		// add TO : REQUIRED
		EmailMessageHelper.setTo(emailMessage, helper);
		// add CC
		EmailMessageHelper.setCc(emailMessage, helper);
		// add BCC
		EmailMessageHelper.setBcc(emailMessage, helper);
		// setSubject : REQUIRED
		EmailMessageHelper.setSubject(emailMessage, helper);
		// setContent
		EmailMessageHelper.setContent(emailMessage, emailMessage.getSimpleContent(), helper);
		// add Attach Resource
		EmailMessageHelper.addAttachment(emailMessage, helper);
		// add Inline Resource
		EmailMessageHelper.addInline(emailMessage, helper);
		// 3. Mail Send
		mailSender.send(mimeMessage);
		
		Map<String, Resource> attatchs = emailMessage.getAttachments();
		String[] key = attatchs.keySet().toArray(new String[0]);
		for (int i = 0; i < attatchs.size(); i++) {
			Resource resource = attatchs.get(key[i]);
			resource.getFile().delete();
		}
	}
}
