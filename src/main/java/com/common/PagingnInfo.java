package com.common;


public class PagingnInfo {

	/** 현재 선택된 페이지 */
	private int currentPageNo = 0;
	
	/** ?개씩 조회 건수 */
	private int recordCountPerPage = 0;
	
	/** 총 갯수 */
	private int totalRecordCount = 0;
	
	/** 총 페이징 수 */
	private int totalPageCount = 0;
	
	/** 조회시 첫 row num*/
	private int firstRecordIndex = 0;

	/** 조회시 마지막 row num*/
	private int lastRecordIndex = 0;
	
	/** 페이징 처리 첫 인뎃스 번호*/
	private int startPageIndex = 0;
	
	public int getStartPageIndex() {
//		if(getTotalPageCount() <= 10 )
//			startPageIndex = 1;
//		else{
//			String temp  = Integer.toString(getTotalPageCount()).substring(0,1);
//			
//		}
		
		if(getCurrentPageNo() < 11)
			startPageIndex = 1;
		else{
			if((getCurrentPageNo()%10) == 0)
				startPageIndex = Integer.parseInt(((getCurrentPageNo()/10)-1) + "1");
			else
				startPageIndex = Integer.parseInt(getCurrentPageNo()/10 + "1");
		}
		return startPageIndex;
	}

	public void setStartPageIndex(int startPageIndex) {
		this.startPageIndex = startPageIndex;
	}

	public int getLastRecordIndex() {
		lastRecordIndex = getCurrentPageNo() * getRecordCountPerPage();
		return lastRecordIndex;
	}

	public void setLastRecordIndex(int lastRecordIndex) {
		this.lastRecordIndex = lastRecordIndex;
	}

	public int getFirstRecordIndex() {
		firstRecordIndex = (getCurrentPageNo() - 1) * getRecordCountPerPage();
		return firstRecordIndex;
	}

	public void setFirstRecordIndex(int firstRecordIndex) {
		this.firstRecordIndex = firstRecordIndex;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}

	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}

	public int getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(int totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public int getTotalPageCount() {
		totalPageCount = ((getTotalRecordCount()-1)/getRecordCountPerPage()) + 1;
		return totalPageCount;
	}

	public void setTotalPageCount(int totalPageCount) {
		this.totalPageCount = totalPageCount;
	}

	
//	public String getCurrentPageNo() {
//		return CurrentPageNo;
//	}
//
//	public void setCurrentPageNo(String currentPageNo) {
//		CurrentPageNo = currentPageNo;
//	}
//
//	public String getRecordCountPerPage() {
//		return RecordCountPerPage;
//	}
//
//	public void setRecordCountPerPage(String recordCountPerPage) {
//		RecordCountPerPage = recordCountPerPage;
//	}
//
//	public String getTotalRecordCount() {
//		return TotalRecordCount;
//	}
//
//	public void setTotalRecordCount(String totalRecordCount) {
//		TotalRecordCount = totalRecordCount;
//	}
//
//	public String getTotalPageCount() {
//		TotalPageCount = Integer.toString(((Integer.parseInt(getTotalRecordCount())-1)/Integer.parseInt(getRecordCountPerPage())) + 1);
//		return TotalPageCount;
//	}
//
//	public void setTotalPageCount(String totalPageCount) {
//		TotalPageCount = totalPageCount;
//	}
//	
	
//	public String getSelectIndex() {
//		return selectIndex;
//	}
//	public void setSelectIndex(String selectIndex) {
//		this.selectIndex = selectIndex;
//	}
//	public String getMaxCount() {
//		return maxCount;
//	}
//	public void setMaxCount(String maxCount) {
//		this.maxCount = maxCount;
//	}
//	public String getPageCount() {
//		pageCount = ((getMaxCount()-1)/getRecordCountPerPage()) + 1;
//		return pageCount;
//	}
//	public void setPageCount(String pageCount) {
//		this.pageCount = pageCount;
//	}
}
