package com.common;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class BasicController {
	
	

	protected PageNavigator makePageNavigator(HttpServletRequest request)
			throws NumberFormatException {

		int currentPage = 1;
		if (StringUtils.isNotBlank(request.getParameter("currentPage"))
				&& StringUtils.isNumeric(request.getParameter("currentPage")))
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		
		int rowsPerPage = Integer.parseInt(Globals.ROWS_PER_PAGE);
		
		if (StringUtils.isNotBlank(request.getParameter("rowsPerPage"))
				&& StringUtils.isNumeric(request.getParameter("rowsPerPage")))
			rowsPerPage = Integer.parseInt(request.getParameter("rowsPerPage"));
		
		if (rowsPerPage > 0)
			request.setAttribute("pagingUnit", "" + rowsPerPage);
		else
			request.setAttribute("pagingUnit", Globals.ROWS_PER_PAGE);

		int pagePerGroup = Integer.parseInt(Globals.PAGE_PER_GROUP);
		if (StringUtils.isNotBlank(request.getParameter("pagePerGroup"))
				&& StringUtils.isNumeric(request.getParameter("pagePerGroup"))) {
			pagePerGroup = Integer.parseInt(request.getParameter("pagePerGroup"));
		}

		PageNavigator pageNavi = new PageNavigator(currentPage, rowsPerPage,
				pagePerGroup);

		return pageNavi;
	}
	
	protected String parseJsonHtmlPage(int rtvList,PageNavigator pageNavi,HttpServletRequest req,String pageFrmIdx){
		StringBuilder sb = new StringBuilder();
		
		int pageTotal = pageNavi.getPageTotal();
		int totalSize = pageNavi.getTotalSize();
		int currentPage = pageNavi.getCurrentPage();
		int rowsPerPage = pageNavi.getRowsPerPage();
		int pageGroupStart = pageNavi.getPageGroupStart();
		int pageGroupEnd = pageNavi.getPageGroupEnd();
		
		String path = req.getContextPath();
		
		if(pageTotal != 1){
			if(totalSize>0){
				sb.append("<div class='paging' id='paging"+pageFrmIdx+"'>");
				if(currentPage-10>=1){
					sb.append("<a href='#none' class='controlPage' onclick='goMoveList"+pageFrmIdx+"(1);return false;'>");						
					sb.append("<img src='"+path+"/common/images/prev.jpg' alt=''>"); //first
					sb.append("</a>");
					
					sb.append("<a href='#none' onclick='goMoveList"+pageFrmIdx+"("+(pageGroupStart-1)+");return false;'>");
					sb.append("<img src='"+path+"/common/images/prev.jpg' alt=''>");
					sb.append("</a>");
				}else if(currentPage-10< 1){
					if(currentPage == 1){
						sb.append("<a href='#none' class='controlPage'>");
						sb.append("<img src='"+path+"/common/images/prev.jpg' alt=''>"); //first
						sb.append("</a>");
						
						sb.append("<a href='#none'>");
						sb.append("<img src='"+path+"/common/images/prev.jpg' alt=''>");
						sb.append("</a>");
					}else{
						sb.append("<a href='#none' class='controlPage' onclick='goMoveList"+pageFrmIdx+"(1);return false;'>");
						sb.append("<img src='"+path+"/common/images/prev.jpg' alt=''>"); //first
						sb.append("</a>");
						
						sb.append("<a href='#none' onclick='goMoveList"+pageFrmIdx+"("+(pageGroupStart-1)+");return false;'>");
						sb.append("<img src='"+path+"/common/images/prev.jpg' alt=''>");
						sb.append("</a>");
					}
				}
				sb.append("<span>");
				
				for(int i = pageGroupStart ; i <= pageGroupEnd ; i++){
					if(i == currentPage) sb.append("<a class='on' href='#none'> "+i+" </a>");
					else sb.append("<a class='' href='#none' onclick='goMoveList"+pageFrmIdx+"("+i+");return false;'> "+i+" </a>");
				}
				sb.append("</span>");
				if(pageGroupStart+10<=pageTotal){
					sb.append("<a href='#none' onclick='goMoveList"+pageFrmIdx+"("+pageGroupStart+10+");return false;'>");
					sb.append("<img src='"+path+"/common/images/next.jpg' alt=''>");
					sb.append("</a>");
					sb.append("<a href='#none' class='controlPage' onclick='goMoveList"+pageFrmIdx+"("+pageTotal+");return false;'>");
					sb.append("<img src='"+path+"/common/images/next.jpg' alt=''> "); //last
					sb.append("</a>");
				}else if(pageGroupStart+10>pageTotal){
					if(currentPage == pageTotal){
						sb.append("<a href='#none'>");
						sb.append("<img src='"+path+"/common/images/next.jpg' alt=''>");
						sb.append("</a>");
						
						sb.append("<a href='#none' class='controlPage' >");
						sb.append("<img src='"+path+"/common/images/next.jpg' alt=''> ");//last
						sb.append("</a>");
					}else{
						sb.append("<a href='#none' onclick='goMoveList"+pageFrmIdx+"(+"+pageTotal+");return false;'>");
						sb.append("<img src='"+path+"/common/images/next.jpg' alt=''>");
						sb.append("</a>");
						
						sb.append("<a href='#none' onclick='goMoveList"+pageFrmIdx+"(+"+pageTotal+");return false;'>");
						sb.append("<img src='"+path+"/common/images/next.jpg' alt=''> "); //last
						sb.append("</a>");
					}
				}
				//<a href="#none" class="btn_excel"><img src="${pageContext.request.contextPath}/common/images/excel.jpg" alt="" /></a>
				sb.append("</div>");
				sb.append("<form id='pageNaviForm"+pageFrmIdx+"' name='pageNaviForm"+pageFrmIdx+"' method='post' style='display:none' action='#none'>");
				sb.append("<input type='hidden' name='currentPage' value='"+currentPage+"' />");
				sb.append("<input type='hidden' name='rowsPerPage' value='"+rowsPerPage+"' />");
				sb.append("</form>");
				
				sb.append("<script type='text/javascript' id='sciprt"+pageFrmIdx+"'>");
				sb.append("var pagingFieldData"+pageFrmIdx+";");
				sb.append("var pagingProperty1"+pageFrmIdx+";");
				
				
				sb.append("function goMoveList"+pageFrmIdx+"(cpage , rowsPerPage) {");
				sb.append("if(pagingFieldData"+pageFrmIdx+"!=null && pagingFieldData"+pageFrmIdx+".length > 0 ) {");
				sb.append("for(var i=0;i<pagingFieldData"+pageFrmIdx+".length;i++){");
				sb.append("var inputTag = document.createElement('INPUT');");
				sb.append("inputTag.type = 'hidden';");
				sb.append("inputTag.name=pagingFieldData"+pageFrmIdx+"[i].fieldName;");
				sb.append("inputTag.value=pagingFieldData"+pageFrmIdx+"[i].fieldValue;");
				sb.append("document.pageNaviForm"+pageFrmIdx+".appendChild(inputTag);");
				sb.append("}}");
				sb.append("document.pageNaviForm"+pageFrmIdx+".currentPage.value = cpage;");
				
				sb.append("if(rowsPerPage != null) {");
				sb.append("document.pageNaviForm"+pageFrmIdx+".rowsPerPage.value = parseInt(rowsPerPage);");
				sb.append("}");
				sb.append(" try {");
				sb.append("if(pagingProperty"+pageFrmIdx+".urlPath != null){");
				sb.append("document.pageNaviForm"+pageFrmIdx+".action = pagingProperty"+pageFrmIdx+".urlPath;");
				sb.append("}");
				sb.append(" } catch(e) {");
				sb.append("document.pageNaviForm"+pageFrmIdx+".action = pagingProperty"+pageFrmIdx+".searchForm.action;");
				sb.append("}");
				sb.append("if(pagingProperty"+pageFrmIdx+".modalName!=null) {");
				sb.append("document.pageNaviForm"+pageFrmIdx+".target = pagingProperty"+pageFrmIdx+".modalName;");
				sb.append("}");
				sb.append("ajaxPageMove(document.pageNaviForm"+pageFrmIdx+")");///////////////////////
				sb.append("}");
				sb.append("function goMoveListPopUp"+pageFrmIdx+"(cpage , rowsPerPage) {");
				sb.append("document.pageNaviForm"+pageFrmIdx+".currentPage.value = cpage;");
				sb.append("if(rowsPerPage != null) {");
				sb.append("document.pageNaviForm"+pageFrmIdx+".rowsPerPage.value = parseInt(rowsPerPage);");
				sb.append("}");
				sb.append("try {");
				sb.append("if(pagingProperty"+pageFrmIdx+".urlPath != null){");
				sb.append("document.pageNaviForm"+pageFrmIdx+".action = pagingProperty"+pageFrmIdx+".urlPath;");
				sb.append("}");
				sb.append("} catch(e) {");
				sb.append("document.pageNaviForm"+pageFrmIdx+".action = pagingProperty"+pageFrmIdx+".searchForm.action;");
				sb.append("}");
				sb.append("document.pageNaviForm"+pageFrmIdx+".target = window.name;");
				sb.append("document.pageNaviForm"+pageFrmIdx+".submit();");//////////////////
				sb.append("}");
				sb.append("</script>");
			/////////////////////
			}
		}
		
		return sb.toString();
	}
	
}
