package com.common;

/**
 *  Class Name : Globals.java
 *  Description : 시스템 구동 시 프로퍼티를 통해 사용될 전역변수를 정의한다.
 *  Modification Information
 *
 *     수정일         수정자                   수정내용
 *   -------    --------    ---------------------------
 *
 *  @author 
 *  @since 
 *  @version 1.0
 *  @see
 *
 */

public class Globals {
	//OS 유형
    public static final String OS_TYPE = EgovProperties.getProperty("Globals.OsType");
    //DB 유형
    public static final String DB_TYPE = EgovProperties.getProperty("Globals.DbType");
    //ShellFile 경로
    public static final String SHELL_FILE_PATH = EgovProperties.getPathProperty("Globals.ShellFilePath");
    //파일 업로드 원 파일명
	public static final String ORIGIN_FILE_NM = "originalFileName";
	//파일 확장자
	public static final String FILE_EXT = "fileExtension";
	//업로드된 파일명
	public static final String UPLOAD_FILE_NM = "uploadFileName";

	public static final String FILE_SIZE = EgovProperties.getProperty("Globals.maxFileSize");
	public static final String FILE_PATH = EgovProperties.getProperty("Globals.defaultFilePath");
	
	public static final String ROWS_PER_PAGE = EgovProperties.getProperty("Globals.rowsPerPage");		
	public static final String PAGE_PER_GROUP = EgovProperties.getProperty("Globals.pagePerGroup");
	
	public static final String MAIL_SENDER = EgovProperties.getProperty("Globals.mailSender");
}
