package com.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import com.util.string.StrUtil;

public class DownloadView extends AbstractView {
	
	@Override
	@SuppressWarnings("unchecked")
	protected void renderMergedOutputModel(Map model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		File file = (File)model.get("downloadFile");
		String m_file = (String)model.get("downloadFileNm");
		if(StrUtil.isNullToEmpty(m_file)) m_file = file.getName();
		
		//한글꺠짐 
		String dfn = URLEncoder.encode(m_file, "UTF-8");
		
		response.setContentType("application/octet-stream");
		response.setContentLength((int)file.length());
		response.setHeader("Content-Disposition", "attachment; fileName=\"" + dfn + "\";");
		response.setHeader("Content-Length", String.valueOf((int)file.length()));
		
		OutputStream out = response.getOutputStream();
		FileInputStream fis = null;
		
		try{
			fis = new FileInputStream(file);
			FileCopyUtils.copy(fis, out);
		}finally{
			if(fis != null){
				try{
					fis.close();
				}catch(IOException ex){}
			}
		}
		out.flush();
	}
}
